<?php
/**
 * Created by abdellah.
 * Date: 16/10/18
 * Time: 14.19
 */

namespace NumberOne\Database;
set_include_path("../Model/");
include_once __DIR__ . ("/../Model/Vote.php");

use \NumberOne\Model\Vote;

class VoteDAO
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insertVote(Vote $vote)
    {
        $userDAO = new UserDAO($this->db);
        $UserID = $userDAO->getIdFromPseudo($vote->getUser()->getPseudo());
        
        $OeuvreID = $vote->getOeuvre()->getID() ;

        $args = [
            'idOeuvre' => $OeuvreID,
            'idUtilisateur' => $UserID,
            'note' => $vote->getNote()
               ];
        // si le vote existe on le modifie ,sinon le crée
        if ($this->VoteExiste($UserID, $OeuvreID)) {
            $sql = "UPDATE Vote SET note=:note WHERE Vote.idOeuvre = :idOeuvre AND Vote.idUtilisateur=:idUtilisateur";
            $result = $this->db->run($sql, $args);
            return $result;
        }
        else {
            $sql = "INSERT INTO Vote (idOeuvre, idUtilisateur, note) 
                  VALUES (:idOeuvre, :idUtilisateur, :note)";
            $result = $this->db->run($sql, $args);
            return $result;
        }   
    }
    
    public function VoteExiste($UserID, $OeuvreID){
        $sql = "SELECT * from Vote WHERE Vote.idOeuvre = :idOeuvre   AND Vote.idUtilisateur=:idUtilisateur ";
        $args = [
            'idOeuvre' => $OeuvreID,
            'idUtilisateur' => $UserID
             ];
        $result = $this->db->run($sql, $args);
        if ($result->fetchColumn() > 0) return True ;
        else return False ;
    }

    public function getVoteValue($userID, $oeuvreID){
        $sql = "SELECT note from Vote WHERE Vote.idOeuvre = :idOeuvre   AND Vote.idUtilisateur=:idUtilisateur ";
        $args = [
            'idOeuvre' => $oeuvreID,
            'idUtilisateur' => $userID
        ];
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
            return false;
        return $result["note"];
    }


    public function getLikesOeuvres ($oeuvreID){
        $args = [
            'IdOeuvre' => $oeuvreID
        ];

        $sql = "SELECT count(*) FROM Vote WHERE Vote.idOeuvre = :IdOeuvre AND Vote.note=1" ;

        $result = $this->db->run($sql, $args)->fetch();;
        return $result["count(*)"];
    }


    public function getDislikesOeuvres ($oeuvreID){
        $args = [
            'IdOeuvre' => $oeuvreID
        ]; 

        $sql = "SELECT count(*) FROM Vote WHERE Vote.idOeuvre = :IdOeuvre AND Vote.note=-1" ;

        $result = $this->db->run($sql, $args)->fetch();
        return $result["count(*)"];
    }

}