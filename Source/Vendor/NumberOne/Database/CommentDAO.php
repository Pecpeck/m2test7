<?php
/**
 * Created by mnauval.
 * Date: 08/10/18
 * Time: 17.06
 */

namespace NumberOne\Database;

use NumberOne\Model\Comment;

set_include_path('../Model/');

class CommentDAO
{
    private $_db;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function getAllComments()
    {
        $sql = "SELECT * FROM Commentaire";
        $result = $this->_db->run($sql)->fetchAll();

        $comments = array();
        foreach ($result as $res)
        {
            $commentID = $res["id"];
            $oeuvreDAO = new OeuvreDAO($this->_db);
            $userDAO = new UserDAO($this->_db);
            $oeuvre = $oeuvreDAO->getOeuvreFromId($res["idOeuvre"]);
            $user = $userDAO->getUserFromId($res["idUtilisateur"]);

            $text = $res["texte"];
            $comment = new Comment($text, $oeuvre, $user);
            $comment->setId($commentID);

            array_push($comments, $comment);
        }
        return $comments;
    }

    public function insertComment($oeuvre, $user, $texte)
    {
        $userDAO = new \NumberOne\Database\UserDAO($this->_db);
        $idUser = $userDAO->getIdFromPseudo($user->getPseudo());
        $args = [
            "idOeuvre" => $oeuvre->getID(),
            "idUtilisateur" => $idUser,
            "texte" => $texte
        ];
        $sql = "INSERT INTO Commentaire (idOeuvre, idUtilisateur, texte) VALUES (:idOeuvre, :idUtilisateur, :texte)";
        $result = $this->_db->run($sql, $args);
        return $result;
    }

    public function getCommentFromId($idComment)
    {
        $args = [
            "id" => $idComment
        ];
        $sql = "SELECT * FROM Commentaire WHERE id = :id";
        $result = $this->_db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        $oeuvreDAO = new OeuvreDAO($this->_db);
        $userDAO = new UserDAO($this->_db);
        $oeuvre = $oeuvreDAO->getOeuvreFromId($result["idOeuvre"]);
        $user = $userDAO->getUserFromId($result["idUtilisateur"]);

        $commentID = $result["id"];
        $text = $result["texte"];
        $comment = new Comment($text, $oeuvre, $user);
        $comment->setId($commentID);
        return $comment;
    }

    public function getTexteFromCommentId($idComment)
    {
        $args = [
            "id" => $idComment
        ];
        $sql = "SELECT * FROM Commentaire WHERE id = :id";
        $result = $this->_db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return $result["texte"];
    }

    public function getOeuvreFromCommentId($idComment)
    {
        $args = [
            "id" => $idComment
        ];
        $sql = "SELECT * FROM Commentaire WHERE id = :id";
        $result = $this->_db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        $oeuvreDAO = new OeuvreDAO($this->_db);
        $oeuvre = $oeuvreDAO->getOeuvreFromId($result["idOeuvre"]);
        return $oeuvre;
    }
}