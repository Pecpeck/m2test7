<?php
/**
 * Created by mnauval.
 * Date: 28/09/18
 * Time: 14.32
 */

namespace NumberOne\Database;
use \PDO;

/**
 * Class Database
 * @package NumberOne\Database
 * adapted from: https://phpdelusions.net/pdo/pdo_wrapper
 */
class Database
{
    private static $HOST_NAME = "m2gl.deptinfo-st.univ-fcomte.fr";

    //private static $HOST_NAME = "localhost";
    private static $PORT_NUMBER = "3306";
    private static $DB_NAME = "m2test7";
    private static $CHARSET = "utf8mb4";
    private static $DB_USER = "m2test7";
    private static $DB_PASS = "m2test7";
    private static $instance;

    private $pdo;

    private function __construct()
    {
        $dsn = "mysql:host=".self::$HOST_NAME.";port=".self::$PORT_NUMBER.";dbname=".self::$DB_NAME.";charset=".self::$CHARSET;
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try
        {
            $this->pdo = new PDO($dsn, self::$DB_USER, self::$DB_PASS, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public static function getInstance()
    {
        if (self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __call($method, $args)
    {
        return call_user_func_array(array($this->pdo, $method), $args);
    }

    public function run($sql, $args = [])
    {
        if (!$args)
        {
            return $this->query($sql);
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public static function deconnect()
    {
        self::$instance = null;
    }
}