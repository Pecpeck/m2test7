<?php
/**
 * Created by mnauval.
 * Date: 30/09/18
 * Time: 12.07
 */

namespace NumberOne\Database;
set_include_path("../Model/");
use \NumberOne\Model\User;

class UserDAO
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Vérifier si un utilisateur existe dans la base de données.
     * @param $pseudo   le pseudo de l'utilisateur
     * @return bool     true si l'utilisateur ayant le pseudo passé en paramètre existe dans la BDD,
     *                  false sinon.
     */
    public function userExists($pseudo)
    {
        $args = [
            'pseudo' => $pseudo
        ];
        $sql = 'SELECT * FROM Utilisateur WHERE pseudo = :pseudo';
        $stmt = $this->db->run($sql, $args);
        $count = $stmt->fetchColumn();
        if ($count > 0)
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Insérer un utilisateur dans la base de données.
     * @param User $user    l'utlisateur à insérer.
     * @return mixed        true si l'opération réussit, false sinon.
     */
    public function insertUser(User $user)
    {
        $args = [
            'pseudo' => $user->getPseudo(),
            'motDePasse' => $user->getHashPassword(),
            'role' => $user->getRole(),
        ];
        $sql = "INSERT INTO Utilisateur (pseudo, motDePasse, role) VALUES (:pseudo, :motDePasse, :role)";
        $result = $this->db->run($sql, $args);
        return $result;
    }

    public function deleteUser($pseudo)
    {
        $args = [
            'pseudo' => $pseudo
        ];
        $sql = "DELETE FROM Utilisateur WHERE pseudo = :pseudo";
        $result = $this->db->run($sql, $args);
        return $result;
    }

    /**
     * Retourner l'id de l'utilisateur dans la BDD à partir de son pseudo.
     * @param $pseudo           le pseudo de l'utilisateur
     * @return int|null         l'id (int) de l'utilisateur si un utilisateur ayant le pseudo passé
     *                          en paramètre existe dans la BDD, null sinon.
     */
    public function getIdFromPseudo($pseudo)
    {
        $args = [
            'pseudo' => $pseudo
        ];
        $sql = "SELECT id FROM Utilisateur WHERE pseudo = :pseudo";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return $result["id"];
    }

    /**
     * Retourner le mot de passe de l'utilisateur à partir de son pseudo.
     * @param $pseudo       le pseudo de l'utilisateur.
     * @return string|null  le mot de passe (string) de l'utilisateur si un utilisateur ayant le pseudo passé
     *                      en paramètre existe dans la BDD, null sinon.
     */
    public function getPasswordFromPseudo($pseudo)
    {
        $args = [
            'pseudo' => $pseudo
        ];
        $sql = "SELECT motDePasse FROM Utilisateur WHERE pseudo = :pseudo";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return $result["motDePasse"];
    }

    /**
     * Retourner un utilisateur à partir de son pseudo.
     * @param $pseudo       le pseudo de l'utilisateur.
     * @return User|null    l'utilisateur (User) si un utilisateur ayant le pseudo passé
     *                      en paramètre existe dans la BDD, null sinon.
     */
    public function getUserFromPseudo($pseudo)
    {
        $args = [
            'pseudo' => $pseudo
        ];
        $sql = "SELECT * FROM Utilisateur WHERE pseudo = :pseudo";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return new User($result["pseudo"], $result["motDePasse"], $result["role"]);
    }

    /**
     * Retourner un utilisateur à partir de son id dans la base de données.
     * @param $id           l'id de l'utilisateur dans la BDD.
     * @return User|null    l'utilisateur (User) si un utilisateur ayant l'id passé
     *                      en paramètre existe dans la BDD, null sinon.
     */
    public function getUserFromId($id)
    {
        $args = [
            'id' => $id
        ];
        $sql = "SELECT * FROM Utilisateur WHERE id = :id";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return new User($result["pseudo"], $result["motDePasse"], $result["role"]);
    }

    /**
     * Retourner tous les utilisateurs enregistrés dans la base de données.
     * @return array    un tableau (array) contenant tous les utilisateurs enregistrés dans la BDD.
     */
    public function getAllUsers()
    {
        $sql = "SELECT * FROM Utilisateur";
        $result = $this->db->run($sql)->fetchAll();

        $users = array();
        foreach ($result as $user)
        {
            $newUser = new User($user["pseudo"], $user["motDePasse"], $user["role"]);
            array_push($users, $newUser);
        }
        return $users;
    }
}