<?php
/**
 * Created by mnauval.
 * Date: 30/09/18
 * Time: 13.56
 */

namespace NumberOne\Database;

set_include_path('../Model/');

use \NumberOne\Model\Category;

class CategoryDAO
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Vérifier si une catégorie existe dans la base de données.
     * @param $libelle  libellé de la catégorie.
     * @return bool     true si la catégorie existe déjà dans la BDD, false sinon.
     */
    public function categoryExists($libelle)
    {
        $args = [
            'libelle' => $libelle
        ];
        $sql = "SELECT * FROM Categorie WHERE libelle = :libelle";
        $result = $this->db->run($sql, $args)->fetchColumn();
        //$result = $this->db->fetchColumn($result);
        if ($result > 0)
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * Insérer une catégorie dans la base de données.
     * @param Category $category    la catégorie à insérer dans la BDD.
     * @return bool                 true si l'opération réussit, false sinon.
     */
    public function insertCategory(Category $category)
    {
        $args =
            [
                'libelle' => $category->getName()
            ];
        $sql = "INSERT INTO Categorie (libelle) VALUES (:libelle)";
        $result = $this->db->run($sql, $args);
        return $result;
    }

    public function deleteCategory($name)
    {
        $args = [
            'libelle' => $name
        ];
        $sql = "DELETE FROM Categorie WHERE libelle = :libelle";
        $result = $this->db->run($sql, $args);
        return $result;
    }

    /**
     * Retourner le libellé d'une catégorie à partir de son id.
     * @param $id           l'id de la catégorie.
     * @return string|null  le libellé (string) de la catégorie si elle existe dans la BDD,
     *                      null sinon.
     */
    public function getLibelleFromId($id)
    {
        $args = [
            'id' => $id
        ];
        $sql = "SELECT libelle FROM Categorie WHERE id = :id";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return $result["libelle"];
    }

    /**
     * Retourner l'id de la catégorie à partir de son libellé.
     * @param $libelle      le libellé de la catégorie.
     * @return int|null     l'id de la catégorie dans la BDD si elle existe,
     *                      null sinon.
     */
    public function getIdFromLibelle($libelle)
    {
        $args = [
            'libelle' => $libelle
        ];
        $sql = "SELECT id FROM Categorie WHERE libelle = :libelle";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return $result["id"];
    }

    /**
     * Retourner une catégorie à partir de son identifiant dans la BDD.
     * @param $id               l'id (integer) de la catégorie.
     * @return Category|null    la catégorie si elle existe dans la BDD,
     *                          null sinon.
     */
    public function getCategoryFromId($id)
    {
        $args = [
            'id' => $id
        ];
        $sql = "SELECT libelle FROM Categorie WHERE id = :id";
        $result = $this->db->run($sql, $args)->fetch();
        if ($result === false)
        {
            return null;
        }
        return new \NumberOne\Model\Category($result["libelle"]);
    }

    /**
     * Retourner toutes les catégories enregistrées dans la BDD.
     * @return array    un array contenant toutes les catégories enregistrées dans la BDD.
     */
    public function getAllCategories()
    {
        $sql = "SELECT * FROM Categorie";
        $result = $this->db->run($sql)->fetchAll();
        $categories = array();
        foreach ($result as $cat)
        {
            $category = new Category($cat["libelle"]);
            array_push($categories, $category);
        }
        return $categories;
    }
}