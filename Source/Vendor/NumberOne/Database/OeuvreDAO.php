<?php
/**
 * Created by mnauval.
 * Date: 30/09/18
 * Time: 14.17
 */

namespace NumberOne\Database;

set_include_path('../Model/');

use NumberOne\Model\Category;
use \NumberOne\Model\Oeuvre;

class OeuvreDAO
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Insérer un oeuvre dans la base de données.
     * @param Oeuvre $oeuvre    Objet du type oeuvre à ajouter dans la BDD.
     * @return bool             true si l'opération réussit, false sinon.
     */
    public function insertOeuvre(Oeuvre $oeuvre)
    {
        $categoryDAO = new CategoryDAO($this->db);
        $categoryID = $categoryDAO->getIdFromLibelle($oeuvre->getCategory()->getName());
        $userDAO = new UserDAO($this->db);
        $userID = $userDAO->getIdFromPseudo($oeuvre->getUser()->getPseudo());
        if ($userID === null)
        {
            return false;
        }
        $args = [
            'idUtilisateur' => $userID,
            'titre' => $oeuvre->getTitle(),
            'description' => $oeuvre->getDescription(),
            'idCategorie' => $categoryID,
            'lien' => $oeuvre->getLink(),
            'date_o' => $oeuvre->getDate()->format("Y-m-d")
        ];

        $sql = "INSERT INTO Oeuvre (idUtilisateur, titre, description, idCategorie, lien, date) 
                  VALUES (:idUtilisateur, :titre, :description, :idCategorie, :lien, :date_o)";
        $result = $this->db->run($sql, $args);
        return $result;
    }

    /**
     * Retourner un oeuvre de la base de données à partir de son identifiant.
     * @param $id               l'id de l'oeuvre dans la BDD.
     * @return Oeuvre|null      Oeuvre ayant l'id passé en paramètre s'il existe dans la BDD, null sinon.
     */
    public function getOeuvreFromId($id)
    {
        $args = [
            'id' => $id
        ];
        $sql = "SELECT * FROM Oeuvre WHERE id = :id";
        $result = $this->db->run($sql, $args)->fetch();

        if ($result === false)
            return null;
        $oeuvreID = $result["id"];
        $title = $result["titre"];
        $userID = $result["idUtilisateur"];
        $userDAO = new \NumberOne\Database\UserDAO($this->db);
        $user = $userDAO->getUserFromId($userID);
        $description = $result["description"];
        $categoryID = $result["idCategorie"];
        $link = $result["lien"];
        if(is_string($result['date']))
        {
            $date_o = new \DateTime($result['date']);
        } else
        {
            $date_o = $result['date'];
        }
        $categoryDAO = new \NumberOne\Database\CategoryDAO($this->db);
        $category = $categoryDAO->getCategoryFromId($categoryID);

        $oeuvre = new \NumberOne\Model\Oeuvre($title, $description, $link, $category, $user, $date_o);
        $oeuvre->setID($oeuvreID);

        return $oeuvre;
    }

    /**
     * Retourner tous les oeuvres de la base de données.
     * @return array    un tableau contenant les objets oeuvres.
     */
    public function getAllOeuvres()
    {
        $sql = "SELECT * FROM Oeuvre";
        $result = $this->db->run($sql)->fetchAll();

        $oeuvres = array();
        foreach ($result as $oeuvre)
        {
            $oeuvreID = $oeuvre["id"];
            $userID = $oeuvre["idUtilisateur"];
            $userDAO = new \NumberOne\Database\UserDAO($this->db);
            $user = $userDAO->getUserFromId($userID);
            $catID = $oeuvre["idCategorie"];
            $catDAO = new CategoryDAO($this->db);
            $category = $catDAO->getCategoryFromId($catID);
            $date_o = $oeuvre["date"];
            if(is_string($oeuvre['date'])){
                $date_o = new \DateTime($oeuvre['date']);
            }
            $newOeuvre = new Oeuvre($oeuvre["titre"], $oeuvre["description"], $oeuvre["lien"], $category, $user, $date_o);
            $newOeuvre->setID($oeuvreID);
            array_push($oeuvres, $newOeuvre);
        }
        return $oeuvres;
    }

    public function getOeuvres(Category $category, \DateTime $date){
        $catDAO = new CategoryDAO($this->db);
        $categoryId = $catDAO->getIdFromLibelle($category->getName());
        $args = [
            'idCategorie' => $categoryId,
            'date' => $date->format("Y-m-d")
        ];
        $sql = "SELECT *, SUM(note) FROM Oeuvre, Vote WHERE Oeuvre.id = Vote.idOeuvre
              AND idCategorie = :idCategorie AND date >= :date GROUP BY Oeuvre.id
              ORDER BY SUM(note) DESC";
        $result = $this->db->run($sql, $args)->fetchAll();

        $oeuvres = array();
        foreach ($result as $oeuvre)
        {
            $oeuvreID = $oeuvre["id"];
            $userID = $oeuvre["idUtilisateur"];
            $userDAO = new UserDAO($this->db);
            $user = $userDAO->getUserFromId($userID);
            $date_o = $oeuvre["date"];
            if(is_string($oeuvre['date'])){
                $date_o = new \DateTime($oeuvre['date']);
            }
            $newOeuvre = new Oeuvre($oeuvre["titre"], $oeuvre["description"], $oeuvre["lien"], $category, $user, $date_o);
            $newOeuvre->setID($oeuvreID);

            array_push($oeuvres, $newOeuvre);
        }
        return $oeuvres;
    }



 }