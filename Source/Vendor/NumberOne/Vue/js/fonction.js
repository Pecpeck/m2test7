//=============================OPEN MODAL==================================
$(document).ready(function(){
    $('.modal').modal();
});

//==========================TEXT AREA======================================
$(document).ready(function() {
    $('input#input_text, textarea#textarea2').characterCounter();
});

$(document).ready(function(){
    $('.tabs').tabs();
});

$( "#addCat" ).click(function() {
    $('.selectCat').css("display", "block");
});

prod = "/~m2test7/prod/Source/Vendor/NumberOne/";

//=============================FONCTIONS FORMULAIRE=======================
(function ($) {
    $(function () {

        //Formulaire de connexion
        $(document).ready(function () {

            $(document).on('submit', '#form_connex', function (e) {
                e.preventDefault();

                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'connexion',
                        username: $("#pseudoConnexion").val(),
                        password: $("#passwordConnexion").val()
                    }, afterConnex);
            });
        });

        //Formulaire d'inscription
        $(document).ready(function () {

            $(document).on('submit', '#form_inscrip', function (e) {
                e.preventDefault();

                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'inscription',
                        username: $("#pseudoInscription").val(),
                        password: $("#passwordInscription").val()
                    }, afterInscription);
            });
        });

        //Formulaire ajout d'oeuvre
        $(document).ready(function () {

            $(document).on('submit', '#form_add', function (e) {
                e.preventDefault();

                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'add_oeuvre',
                        title: $("#titre").val(),
                        description: $("#description").val(),
                        category: $("#categorie").val(),
                        link: $("#lien").val()
                    }, afterAdd);
            });
        });

        //Formulaire ajout de category
        $(document).ready(function () {

            $(document).on('submit', '#form_ajout_cat', function (e) {
                e.preventDefault();

                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'add_category',
                        name: $("#nomCategory").val()
                    }, afterAddCate);
            });
        });

    });

    //Affichage des catégories
    $(document).ready(function(){
        $('.selectCat').formSelect();
        $('.selectCat').css("display", "block");
        $.post(prod + 'Controller/Intermediaire.php',
            {
                use: 'get_category'
            }, afficheCate);

    });

    //Affichage des catégories admin
    $(document).ready(function(){
        $('.selectCat').formSelect();
        $('.selectCat').css("display", "block");
        $.post(prod + 'Controller/Intermediaire.php',
            {
                use: 'get_category_admin'
            }, afficheCateAdmin);

    });


    //Affichage des utilisateurs
    $(document).ready(function(){
        $.post(prod + 'Controller/Intermediaire.php',
            {
                use: 'get_all_user'
            }, afficheAllUser);

    });

    //Filtre des oeuvres
    $(document).ready(function(){
        $(document).on('submit', '#form_filtre', function (e) {
            e.preventDefault();
            console.log($("#categorieAll").val());
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'filtre_oeuvre',
                    category: $("#categorieAll").val(),
                }, afficheOeuvreFiltre);
        });

    });

    //Filtre des oeuvres hebdo
    $(document).ready(function(){
        $(document).on('submit', '#form_filtre_hebdo', function (e) {
            e.preventDefault();
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'filtre_oeuvre_hebdo',
                    category: $("#categorieAllHebdo").val(),
                }, afficheOeuvreFiltreHebdo);
        });

    });

    //Filtre des oeuvres mensu
    $(document).ready(function(){
        $(document).on('submit', '#form_filtre_mensu', function (e) {
            e.preventDefault();
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'filtre_oeuvre_mensu',
                    category: $("#categorieAllMensu").val(),
                }, afficheOeuvreFiltreMensu);
        });

    });

    //Filtre des oeuvres annuel
    $(document).ready(function(){
        $(document).on('submit', '#form_filtre_annuel', function (e) {
            e.preventDefault();
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'filtre_oeuvre_annuel',
                    category: $("#categorieAllAnnuel").val(),
                }, afficheOeuvreFiltreAnnuel);
        });

    });

    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("id");
    if(c != null) {
        //Affichage d'une oeuvre
        $(document).ready(function () {
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'get_oeuvre_solo',
                    id: c
                }, afficheOeuvreSolo);
        });
    }
    //Affichage des oeuvres
    reloadOeuvre();
    reloadOeuvreMensuel();
    reloadOeuvreAnnuel();
    reloadOeuvreHebdo();

    //Affichage Navbar
    reloadNavbar();
    //Affichage vote
    if (window.location.search != "")
        reloadVote();

    //Vote Positif
    $(document).ready(function () {
        var id = $_GET("id");
        console.log(id);
        $("#votePositif").click(function () {
            if(document.getElementById("votePositif").classList.contains("grey")){
                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'vote',
                        vote_value: 1,
                        id: id
                    }, afterVotePositif);
            }else if(document.getElementById("votePositif").classList.contains("green")){
                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'vote',
                        vote_value: 0,
                        id: id
                    }, afterVotePositif);
            }
        });
    });

    //Vote Negatif
    $(document).ready(function () {
        var id = $_GET("id");
        console.log(id);
        $("#voteNegatif").click(function () {
            if(document.getElementById("voteNegatif").classList.contains("grey")){
                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'vote',
                        vote_value: -1,
                        id: id
                    }, afterVotePositif);
            }else if(document.getElementById("voteNegatif").classList.contains("red")){
                $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'vote',
                        vote_value: 0,
                        id: id
                    }, afterVoteNegatif);
            }
        });
    });

    //Delete user
    $(document).ready(function () {
        $(document).on('click', '.deleteUser', function (event) {
            var username = $(event.target).parent().parent().text().substring(0, $(event.target).parent().parent().text().length - 5)
            console.log(username);
            $.post(prod + 'Controller/Intermediaire.php',
                    {
                        use: 'delete_user',
                        pseudo: username
                    }, afterDeleteUser);
        });
    });


    //Delete catégorie
    $(document).ready(function () {
        $(document).on('click', '.deleteCategory', function (event) {
            var catname = $(event.target).parent().parent().text().substring(0, $(event.target).parent().parent().text().length - 5)
            console.log(catname);
            $.post(prod + 'Controller/Intermediaire.php',
                {
                    use: 'delete_category',
                    name: catname
                }, afterDeleteCategory);
        });
    });

})(jQuery);




//=============================FONCTIONS CALLBACK=======================

//Fonction callback pour suppression catégorie
function afterDeleteCategory(data) {
    if (data.trim() == "OK") {
        M.toast({html: 'Vous avez supprimez la catégorie!', classes: 'rounded'});
        reloadCateList();
    }
    else {
        M.toast({html: 'Erreur lors de la suppression : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour suppression user
function afterDeleteUser(data) {
    if (data.trim() == "OK") {
        M.toast({html: 'Vous avez supprimez l\'utilisateur!', classes: 'rounded'});
        reloadUserList();
    }
    else {
        M.toast({html: 'Erreur lors de la suppression : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour un vote positif
function afterVotePositif(data) {
    if (data.trim() == "OK") {
        M.toast({html: 'Votre vote à été pris en compte!', classes: 'rounded'});
        reloadVote();
    }
    else {
        M.toast({html: 'Erreur lors du vote : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour un vote negatif
function afterVoteNegatif(data) {
    if (data.trim() == "OK") {
        M.toast({html: 'Votre vote à été pris en compte!', classes: 'rounded'});
        reloadVote();
    }
    else {
        M.toast({html: 'Erreur lors du vote : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour ajout d'une oeuvre
function afterAdd(data) {
    if (data.trim() == "OK") {
        // Le membre est connecté. Ajoutons lui un message dans la page HTML.
        reloadOeuvre();
        $('.modal').modal('close');
        M.toast({html: 'Votre oeuvre a été ajouté !', classes: 'rounded'});
    }
    else {
        M.toast({html: 'Erreur lors de l\'ajout : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour ajout d'une catégorie
function afterAddCate(data) {
    if (data.trim() == "OK") {
        // Le membre est connecté. Ajoutons lui un message dans la page HTML.
        reloadCateList();
        M.toast({html: 'Votre categorie a été ajouté !', classes: 'rounded'});
    }
    else {
        M.toast({html: 'Erreur lors de l\'ajout : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour la connexion
function afterConnex(data) {
    if (data.trim() == "OK") {
        // Le membre est connecté. Ajoutons lui un message dans la page HTML.
        reloadNavbar();
        $('.modal').modal('close');
        M.toast({html: 'Vous avez été connecté avec succès !', displayLength: 6000, classes: 'rounded'});
    }
    else {
        M.toast({html: 'Erreur lors de la connection : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback pour l'inscription
function afterInscription(data) {
    if (data.trim() == "OK") {
        $('.modal').modal('close');
        M.toast({html: 'Vous avez été inscrit avec succès !', displayLength: 6000, classes: 'rounded'});
    }
    else {
        M.toast({html: 'Erreur lors de l\'inscription : '+data, displayLength: 6000, classes: 'rounded'});
    }
};

//Fonction callback de la recupération des catégories pour liste de selection
function afficheCate(data){
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        $('#categorie').append('<option value="'+element+'">'+element+'</option>');
        $('#categorieAll').append('<option value="'+element+'">'+element+'</option>');
        $('#categorieAllHebdo').append('<option value="'+element+'">'+element+'</option>');
        $('#categorieAllMensu').append('<option value="'+element+'">'+element+'</option>');
        $('#categorieAllAnnuel').append('<option value="'+element+'">'+element+'</option>');
    });
}

//Fonction callback de la recupération des oeuvres
function afficheOeuvre(data){
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

        }else{
            $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres hebdo
function afficheOeuvreHebdo(data){
    console.log(JSON.parse(data));
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

        }else{
            $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres Mensuel
function afficheOeuvreMensu(data){
    console.log(JSON.parse(data));
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

        }else{
            $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres Annuel
function afficheOeuvreAnnuel(data){
    console.log(JSON.parse(data));
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

        }else{
            $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des utilisateurs
function afficheAllUser(data){
    var userArray = JSON.parse(data);
    userArray.forEach(function(element) {
        if(element[0] != 'admin')
        $('#listUtilisateur').append('<li class="collection-item">Pseudo :<div>'+element[0]+'<a  href="#!" class="secondary-content deleteUser"><i class="material-icons">clear</i></a></div></li>');
    });
}
//Fonction callback de la recupération des catégories pour l'administrateur
function afficheCateAdmin(data){
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
            $('#listCategory').append('<li class="collection-item">Nom : <div>'+element+'<a  href="#!" class="secondary-content deleteCategory"><i class="material-icons">clear</i></a></div></li>');
    });
}

//Fonction callback de la recupération d'une seule oeuvre pour l'afficher
function afficheOeuvreSolo(data){
    var dataArr = JSON.parse(data);
    $('#titreOeuvreSolo').text(dataArr[0]);
    if(Url_Valide(dataArr[1])) {
        $('#imgOeuvreSolo').attr('src', dataArr[1]);
    }else{
        $('#imgOeuvreSolo').attr('src', "");
    }
    $('#imgOeuvreSolo').attr('alt', 'Ceci est une image');
    $('#descOeuvreSolo').text(dataArr[2]);
    $('#catOeuvreSolo').text(dataArr[3]);
    $('#lienOeuvreSolo').attr('href', dataArr[1]);
    $('#lienOeuvreSolo').text(dataArr[1]);
    $('#dateOeuvreSolo').text(dataArr[4]);

}

//Fonction callback de la recupération des oeuvres filtrer
function afficheOeuvreFiltre(data){
    $('#listOeuvre').empty();
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }else{
            $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres filtrer hebdo
function afficheOeuvreFiltreHebdo(data){
    $('#listOeuvreHebdo').empty();
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }else{
            $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres filtrer mensu
function afficheOeuvreFiltreMensu(data){
    $('#listOeuvreMensu').empty();
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }else{
            $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction callback de la recupération des oeuvres filtrer mensu
function afficheOeuvreFiltreAnnuel(data){
    $('#listOeuvreAnnuel').empty();
    var catArray = JSON.parse(data);
    catArray.forEach(function(element) {
        if(Url_Valide(element[1])) {
            $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }else{
            $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="" alt="" class="circle"/><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
        }
    });
}

//Fonction de reset du filtre des oeuvres
function resetOeuvre(){
    $('#listOeuvre').empty();
    $('#categorieAll').val('');
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre' }, function (data) {
        var catArray = JSON.parse(data);
        catArray.forEach(function(element) {
            if(Url_Valide(element[1])) {
                $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

            }else{
                $('#listOeuvre').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
            }
        });
    });
}

//Fonction de reset du filtre des oeuvres hebdo
function resetOeuvreHebdo(){
    $('#listOeuvreHebdo').empty();
    $('#categorieAllHebdo').val('');
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_hebdo' }, function (data) {
        var catArray = JSON.parse(data);
        catArray.forEach(function(element) {
            if(Url_Valide(element[1])) {
                $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

            }else{
                $('#listOeuvreHebdo').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
            }
        });
    });
}

//Fonction de reset du filtre des oeuvres hebdo
function resetOeuvreMensu(){
    $('#listOeuvreMensu').empty();
    $('#categorieAllMensu').val('');
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_mensu' }, function (data) {
        var catArray = JSON.parse(data);
        catArray.forEach(function(element) {
            if(Url_Valide(element[1])) {
                $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

            }else{
                $('#listOeuvreMensu').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
            }
        });
    });
}

//Fonction de reset du filtre des oeuvres hebdo
function resetOeuvreAnnuel(){
    $('#listOeuvreAnnuel').empty();
    $('#categorieAllAnnuel').val('');
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_annuel' }, function (data) {
        var catArray = JSON.parse(data);
        catArray.forEach(function(element) {
            if(Url_Valide(element[1])) {
                $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="' + element[1] + '" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="' + element[1] + '">' + element[1] + '</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');

            }else{
                $('#listOeuvreAnnuel').append('<li class="collection-item avatar"><div><img src="" alt="Ceci est une image" class="circle"><span class="title">Titre: ' + element[0] + '</span><p>Lien:<br><a href="">Aucun lien vers l\'oeuvre</a></p><p>Description:<br>' + element[2] + '</p><a onclick="decl()" href="' + prod + 'Vue/php/affichageOeuvre.php?id=' + element[3] + '" title="Détail de l\'oeuvre" name="" class="secondary-content"><i class="material-icons">thumbs_up_down</i></a></div></li>');
            }
        });
    });
}


//=============================FONCTIONS REFRESH=======================

//Fonction pour refresh la navbar
function reloadNavbar() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_bar' }, function (data) {
        $('#navbar').html(data);
    });
}
//Fonction pour refresh les oeuvres
function reloadOeuvre() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre' }, function (data) {
        $('#oeuvreDiv').empty();
        $('#oeuvreDiv').html('<ul id="listOeuvre" class="collection"></ul>');
        $('#oeuvreDiv').html(afficheOeuvre(data));
    });
}

//Fonction pour refresh les oeuvres hebdo
function reloadOeuvreHebdo() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_hebdo' }, function (data) {
        $('#oeuvreDivHebdo').empty();
        $('#oeuvreDivHebdo').html('<ul id="listOeuvreHebdo" class="collection"></ul>');
        $('#oeuvreDivHebdo').html(afficheOeuvreHebdo(data));
    });
}

//Fonction pour refresh les oeuvres annuel
function reloadOeuvreAnnuel() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_annuel' }, function (data) {
        $('#oeuvreDivAnnuel').empty();
        $('#oeuvreDivAnnuel').html('<ul id="listOeuvreAnnuel" class="collection"></ul>');
        $('#oeuvreDivAnnuel').html(afficheOeuvreAnnuel(data));
    });
}

//Fonction pour refresh les oeuvres mensuel
function reloadOeuvreMensuel() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_oeuvre_mensu' }, function (data) {
        $('#oeuvreDivMensu').empty();
        $('#oeuvreDivMensu').html('<ul id="listOeuvreMensu" class="collection"></ul>');
        $('#oeuvreDivMensu').html(afficheOeuvreMensu(data));
    });
}

//Fonction pour refresh les votes
function reloadVote() {
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_vote', id: $_GET("id") }, function (data) {
        var tabVote = JSON.parse(data);
        if(tabVote[2] == 0) {
            document.getElementById("votePositif").classList.remove('green');
            document.getElementById("votePositif").classList.add('grey');
            document.getElementById("voteNegatif").classList.remove('red');
            document.getElementById("voteNegatif").classList.add('grey');
            document.getElementById("votePositif").innerHTML = '<i class="left large material-icons">thumb_up</i>'+ tabVote[0];
            document.getElementById("voteNegatif").innerHTML = '<i class="left large material-icons">thumb_down</i>'+ tabVote[1];
        }else if(tabVote[2] == 1){
            document.getElementById("voteNegatif").classList.remove('red');
            document.getElementById("voteNegatif").classList.add('grey');
            document.getElementById("votePositif").classList.remove('grey');
            document.getElementById("votePositif").classList.add('green');
            document.getElementById("votePositif").innerHTML = '<i class="left large material-icons">thumb_up</i>'+ tabVote[0];
            document.getElementById("voteNegatif").innerHTML = '<i class="left large material-icons">thumb_down</i>'+ tabVote[1];
        }else{
            document.getElementById("votePositif").classList.remove('green');
            document.getElementById("votePositif").classList.add('grey');
            document.getElementById("voteNegatif").classList.remove('grey');
            document.getElementById("voteNegatif").classList.add('red');
            document.getElementById("votePositif").innerHTML = '<i class="left large material-icons">thumb_up</i>'+ tabVote[0];
            document.getElementById("voteNegatif").innerHTML = '<i class="left large material-icons">thumb_down</i>'+ tabVote[1];
        }
    });
}

//Fonction refresh userList
function reloadUserList(){
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_all_user'}, function (data) {
        var userArray = JSON.parse(data);
        $('#listUtilisateur').empty();
        userArray.forEach(function(element) {
            if(element[0] != 'admin')
                $('#listUtilisateur').append('<li class="collection-item">Pseudo :<div>'+element[0]+'<a  href="#!" class="secondary-content deleteUser"><i class="material-icons">clear</i></a></div></li>');
        });
    });
}

function reloadCateList(){
    $.post(prod + 'Controller/Intermediaire.php', { use: 'get_category_admin'}, function (data) {
        var catArray = JSON.parse(data);
        $('#listCategory').empty();
        catArray.forEach(function (element) {
            $('#listCategory').append('<li class="collection-item">Nom : <div>' + element + '<a href="#!" class="secondary-content deleteCategory"><i class="material-icons">clear</i></a></div></li>');
        });
    });
}


//Verification d'url
function Url_Valide(UrlTest) {
    var regexp = new RegExp("^((http|https):\/\/){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|\/|=|?|%|&)?]+)+$");
    return regexp.test(UrlTest);
}

//Recupère Get element
function $_GET(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}