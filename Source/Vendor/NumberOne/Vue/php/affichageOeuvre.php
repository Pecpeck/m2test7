<?php
/**
 * Created by IntelliJ IDEA.
 * User: kristopher
 * Date: 16/10/18
 * Time: 10:20
 */
session_start();
require_once 'display.php';
?>
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
    <body>
    <!-- Barre de navigation/Bouton ajout oeuvre-->
    <div id="navbar">
        <?php navbarSet() ?>
    </div>

    <?php displayOeuvre() ?>
    <!-- Modal Structure Connexion -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Connexion</h4>
            <div class="row">
                <form id="form_connex" method="post" class="col s6">
                    <div class="row">
                        <div class="input-field col s12">
                            <input  id="pseudoConnexion" type="text" class="validate">
                            <label for="pseudoConnexion">Pseudo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="passwordConnexion" type="password" class="validate">
                            <label for="passwordConnexion">Password</label>
                        </div>
                    </div>
                    <button id="submit" class="right blue btn waves-effect waves-light" type="submit">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </form>
            </div>
        </div>
    </div>

    <div id="modal2" class="modal">
        <div class="modal-content">
            <h4>Inscription</h4>
            <div class="row">
                <form id="form_inscrip" method="post" class="col s6">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="pseudoInscription" type="text" class="validate">
                            <label for="pseudoInscription">Pseudo</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="passwordInscription" type="password" class="validate">
                            <label for="passwordInscription">Password</label>
                        </div>
                    </div>
                    <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>
                </form>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script type="text/javascript" src="../js/fonction.js"></script>
    </body>
</html>