<?php


function navbarSet(){

    $res ='<nav><div class="nav-wrapper green">'.
        '<a href="http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test7/prod/index.php" class="brand-logo center">Numero Number One</a>';


    if(isset($_SESSION['role']) && $_SESSION['role'] == "admin"){
        $res .= '<ul class="right hide-on-med-and-down">'.
                    '<li><a class="purple waves-effect waves-light btn-large" href="/~m2test7/prod/Source/Vendor/NumberOne/Vue/php/administration.php"><i class="material-icons right">settings_applications</i>Administrer</a></li>'.
                    '<li><a class="red waves-effect waves-light btn-large" href="/~m2test7/prod/Source/Vendor/NumberOne/Vue/php/deconnexion.php"><i class="material-icons right">person_outline</i>Déconnexion</a></li>'.
                '</ul>';
    }else if(isset($_SESSION['pseudo'])){
        $res .= '<ul class="right hide-on-med-and-down">'.
                    '<li><a class="red waves-effect waves-light btn-large" href="/~m2test7/prod/Source/Vendor/NumberOne/Vue/php/deconnexion.php"><i class="material-icons right">person_outline</i>Déconnexion</a></li>'.
                '</ul>';
    }else{
        $res .= '<ul class="right hide-on-med-and-down">'.
            '<li><a id="boutonInscription" class="orange waves-effect waves-light btn-large modal-trigger" href="#modal2"><i class="material-icons right">control_point</i>Inscription</a></li>' .
                    '<li><a id="boutonConnexion" class="blue waves-effect waves-light btn-large modal-trigger" href="#modal1"><i class="material-icons right">person</i>Connexion</a></li>'.
                '</ul>';
    }
    $res .= '</a></div></nav>';
    echo $res;

    if(isset($_SESSION['pseudo'])){
        echo '<div class="fixed-action-btn">
                <a id="addCat" class="btn-floating btn-large red modal-trigger" href="#modalO" title="Ajouter une oeuvre">
                    <i class="large material-icons">add</i>
                </a>
              </div>';
    }
}

function utilisateurSet(){
    echo '<ul id="listUtilisateur" class="collection"></ul>';
}

function categorySet(){
    echo '<ul id="listCategory" class="collection"></ul>';
}

function displayOeuvre(){

    if(isset($_GET['id'])){
        echo '<div class="row">'.
            '<div class="col s12 m10">'.
                '<div class="card horizontal">'.
                    '<div class="card-image ">'.
                        '<img id="imgOeuvreSolo" src="">'.
                    '</div>'.
                    '<div class="card-content">'.
                        '<h4 id="titreOeuvreSolo" class="card-title"></h4>'.
                        '<label for="catOeuvreSolo">Catégorie :</label>'.
                        '<p id="catOeuvreSolo"></p>'.
                        '<label for="descOeuvreSolo">Description :</label>'.
                        '<p id="descOeuvreSolo"></p>'.
                        '<label for="descOeuvreSolo">Date de publication :</label>'.
                        '<p id="dateOeuvreSolo"></p>'.
                        '<label for="descOeuvreSolo">Lien :</label><br>'.
                        '<a id="lienOeuvreSolo" href="#"></a><br>'.
                        '<label for="vote">Votes :</label><br>'.
                        '<div id="vote">
                            <a id="votePositif" class="btn-small waves-effect waves-light grey"  title="Positif"><i class="left large material-icons">thumb_up</i></a>
                            <a id="voteNegatif" class="btn-small waves-effect waves-light grey"  title="Negatif"><i class="left large material-icons">thumb_down</i></a>
                         </div>'.
                    '</div>'.
                '</div>'.
           ' </div>'.
       ' </div>';
    }
}


?>


