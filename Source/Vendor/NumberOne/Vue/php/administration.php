<?php
/**
 * Created by IntelliJ IDEA.
 * User: kristopher
 * Date: 16/10/18
 * Time: 10:20
 */
session_start();
require_once 'display.php';
?>
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<!-- Barre de navigation/Bouton ajout oeuvre-->
<div id="navbar">

</div>

<ul id="tabs-swipe-demo" class="tabs green">
    <li class="tab col s3"><a class="active white-text" href="#allUser">Utilisateur</a></li>
    <li class="tab col s3"><a class="white-text" href="#allCat">Catégorie</a></li>
</ul>
<div id="allUser" class="col s12">
    <?php utilisateurSet(); ?>
</div>
<div id="allCat" class="col s12">
    <form id="form_ajout_cat" method="post" action="#" class="col s4">
        <label for="nomCategory">Ajouter Catégorie</label>
        <input  id="nomCategory" type="text" class="validate">
        <button href="#" class="left red btn waves-effect waves-light" type="submit" name="action">Ajouter
            <i class="material-icons right">add</i>
        </button>
    </form>
    <br><br>
    <?php categorySet(); ?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script type="text/javascript" src="../js/fonction.js"></script>
</body>
</html>