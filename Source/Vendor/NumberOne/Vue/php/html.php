<?php

function html_header($deco = 0) {

echo '<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>';

echo '<header>';
//Si admin
if ($deco == 2) {

}
//Si connecter
if ($deco == 1){
    echo '
    <nav>
    <div class="nav-wrapper green">
      <a href="#!" class="brand-logo center">Numero Number One</a>
      <ul class="right hide-on-med-and-down">
        <li><a class="red waves-effect waves-light btn-large" href="deconnexion.php"><i class="material-icons right">person_outline</i>Déconnexion</a></li>
      </ul>
    </div>
  </nav>
  <!-- Modal Structure Connexion -->
  <div id="modal1" class="modal">
      <div class="modal-content">
        <h4>Connexion</h4>
        <div class="row">
            <form method="post" action="connexion.php" class="col s6">
              <div class="row">
                <div class="input-field col s12">
                  <input  id="pseudo" type="text" class="validate">
                  <label for="pseudo">Pseudo</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="password" type="password" class="validate">
                  <label for="password">Password</label>
                </div>
              </div>
              <button href="#" id="submitConnex" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
    </div>';
}
//Si non connecter
if ($deco == 0){
    echo '
    <nav>
    <div class="nav-wrapper green">
      <a href="#!" class="brand-logo center">Numero Number One</a>
      <ul class="right hide-on-med-and-down">
        <li><a class="orange waves-effect waves-light btn-large modal-trigger" href="#modal2"><i class="material-icons right">control_point</i>Inscription</a></li>
        <li><a class="blue waves-effect waves-light btn-large modal-trigger" href="#modal1"><i class="material-icons right">person</i>Connexion</a></li>
      </ul>
    </div>
  </nav>
  <!-- Modal Structure Connexion -->
  <div id="modal1" class="modal">
      <div class="modal-content">
        <h4>Connexion</h4>
        <div class="row">
            <form method="post" action="connexion.php" class="col s6">
              <div class="row">
                <div class="input-field col s12">
                  <input  id="pseudoC" type="text" class="validate">
                  <label for="pseudoC">Pseudo</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="passwordC" type="password" class="validate">
                  <label for="passwordC">Password</label>
                </div>
              </div>
              <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
    </div>
    <!-- Modal Structure inscription -->
    <div id="modal2" class="modal">
      <div class="modal-content">
        <h4>Inscription</h4>
        <div class="row">
            <form method="post" action="connexion.php" class="col s6">
              <div class="row">
                <div class="input-field col s12">
                  <input  id="pseudoI" type="text" class="validate">
                  <label for="pseudoI">Pseudo</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="passwordI" type="password" class="validate">
                  <label for="passwordI">Password</label>
                </div>
              </div>
              <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
    </div>';
}
echo '</header>
        <body>';
}

function html_middle(){
    echo '';
}

function html_end() {
    echo 'oeuvre</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m6">
                    <select class=".selectCat" name="filtre-categorie">
                        <option value="" disabled selected>Choisissez votre catégorie</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                    <label for="filtre-categorie">Catégorie</label>
                </div>
              </div>
              <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </form>
          </div>
        </div>
    </div>    
    
            <script type="text/javascript" src="../js/fonction.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        </body>
    </html>
    
    ';
}
?>
