<?php
/**
 * Created by PhpStorm.
 * User: Razer
 * Date: 09/10/2018
 * Time: 17:39
 */

namespace NumberOne\Controller;


// Cette classe recense les différents messages d'erreurs qui peuvent d'être envoyer à la vue.

class Error
{
    // No error
    public static $NoError = 'OK';

    // Authenfication
    public static $AuthFailed = 'Connexion refusée : Votre pseudo/mot de passe est incorrecte.';
    public static $NotConnectedUser = 'Vous n\'êtes pas connecté, vous n\'avez pas accès à cette fonctionalité.';

    // Pseudo
    public static $EmptyPseudo = 'Veuillez saisir un pseudo.';
    public static $SyntaxPseudo = 'Votre pseudo doit avoir entre 4 et 16 caractères parmi les caractères suivants a-z, A-Z et 0-9, dont au moins une lettre';
    public static $UndefinedPseudo = 'Le pseudo est inconnu.';
    public static $ExistingPseudo = 'Le pseudo est déjà utilisé. Choisissez en un nouveau.';

    // Password
    public static $EmptyPassword = 'Veuillez saisir un mot de passe.';
    public static $SyntaxPassword = 'Votre mot de passe doit avoir entre 8 caractères et 50 parmi les caractères suivants a-z, A-Z et 0-9';

    // Title
    public static $EmptyTitle = 'Veuillez saisir un titre.';
    public static $SyntaxTitle = 'Le titre doit avoir entre 1 caractères et 50';

    // Description
    public static $EmptyDescription = 'Veuillez saisir une description.';
    public static $SyntaxDescription = 'La description doit avoir entre 50 caractères et 2048';

    // Sumbit
    public static $SumbittingOeuvreError = 'Erreur lors de l\'ajout de l\'oeuvre.';
    public static $SumbittingCategoryError = 'Erreur lors de l\'ajout de la catégorie.';
    public static $SumbittingVoteError = 'Erreur lors de l\'ajout du vote.';

    // Other
    public static $UndefinedCategory = 'La catégorie n\'existe pas.';
    public static $ErrorNote = 'La note doit être 1, 0 ou -1';
    public static $UnvalidDate = 'La date n\'est pas une date valide';
    public static $ErrorDeleteUser = 'Erreur lors de la suppression.';
    public static $ErrorDeleteCate = 'Erreur lors de la suppression.';

}