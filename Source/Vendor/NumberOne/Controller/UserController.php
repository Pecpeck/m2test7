<?php
/**
 * Created by IntelliJ IDEA.
 * User: kristopher
 * Date: 23/10/18
 * Time: 09:50
 */

namespace NumberOne\Controller ;


include_once __DIR__ . ("/../Database/Database.php");
include_once __DIR__ . ("/../Database/UserDAO.php");
include_once __DIR__ . ("/../Model/User.php");

use \NumberOne\Model\User;
use \NumberOne\Database\UserDAO;


class UserController
{
    function deleteUser($pseudo, $dao){

        if ($dao->deleteUser($pseudo))
            return Error::$NoError;
        return Error::$ErrorDeleteUser;

    }

}