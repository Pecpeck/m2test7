<?php
/**
 * Created by Victor
 * Date: 02/10/2018
 * Time: 12:11
 */

namespace NumberOne\Controller;

include_once __DIR__ . ('/../Database/Database.php');
include_once __DIR__ . ("/../Database/OeuvreDAO.php");
include_once __DIR__ . ("/../Database/CategoryDAO.php");
include_once __DIR__ . ("/../Database/UserDAO.php");
include_once __DIR__ . ("/../Model/Oeuvre.php");
include_once __DIR__ . ("/../Model/Category.php");
include_once __DIR__ . ("/../Model/User.php");



use \NumberOne\Model\Oeuvre;
use \NumberOne\Model\Category;
use \NumberOne\Model\User;
use \NumberOne\Database\OeuvreDAO;



class OeuvreController
{
    /**
     * Ajout d'une nouvelle oeuvre.
     * @param \ArrayObject $req     Les données envoyée sur l'oeuvre
     * @param User $user            L'utilisateur connecté
     * @param OeuvreDAO $dao        Classe de communication avec la base
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function submit($req, User $user, $dao)
    {
        $title = trim($req['title']);
        $desc = trim($req['description']);
        $link = trim($req['link']);
        $cat = new Category($req['category']);
        $time = new \DateTime();
        $oeuvre = new Oeuvre($title, $desc, $link, $cat, $user,$time);
        $error = self::validateOeuvre($title, $desc);
        if ($error != Error::$NoError)
            return $error;
        if (empty($user))
            return Error::$NotConnectedUser;
        if ($dao->insertOeuvre($oeuvre))
            return Error::$NoError;
        return Error::$SumbittingOeuvreError;
    }

    /**
     * Vérifie les données d'une oeuvre
     * @param string $title         Le titre de l'oeuvre.
     * @param string $desc          La description de l'oeuvre
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function validateOeuvre ($title, $desc){
        if(!isset($desc) OR empty($desc) OR $desc=="")
            return Error::$EmptyDescription;
        if(strlen($desc) < 50 or strlen($desc) > 2048)
            return Error::$SyntaxDescription;
        if(!isset($title) OR empty($title) OR $title=="")
            return Error::$EmptyTitle;
        if(strlen($title) < 1 or strlen($title) > 50)
            return Error::$SyntaxTitle;
        return Error::$NoError;
    }
}
?>
