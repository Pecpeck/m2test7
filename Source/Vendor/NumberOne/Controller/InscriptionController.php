<?php 
namespace NumberOne\Controller ;


include_once __DIR__ . ("/../Database/Database.php");
include_once __DIR__ . ("/../Database/UserDAO.php");
include_once __DIR__ . ("/../Model/User.php");
use \NumberOne\Model\User;
use \NumberOne\Database\UserDAO;

/**
 * Inscription d'un utilisateur.
 * @param UserDAO $userDAO      Classe de communication avec la base
 * @param string $pseudo        Le pseudo de l'utilisateur.
 * @param string $pass          Le mot de passe de l'utilisateur (en clair)
 * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
 */
class InscriptionController {

    public static function createUser($userDAO,$pseudo,$pass) {
       $errorMessage=inscriptionController::validateUser($pseudo,$pass,$userDAO)  ;
       if ($errorMessage==Error::$NoError)
       {
       
          // Hachage du mot de passe
          $pass_hash = password_hash($pass,PASSWORD_DEFAULT);
          $user1= new User($pseudo,$pass_hash,\NumberOne\Model\EnumRole::$User) ;

          // Enregistrement du membre dans la bdd
          $userDAO->insertUser($user1);

          return Error::$NoError ;
       }
       else return $errorMessage ;
    
    }

    /**
     * Vérifie les données d'inscription
     * @param string $pseudo        Le pseudo de l'utilisateur.
     * @param string $pass          Le mot de passe de l'utilisateur (en clair)
     * @param UserDAO $userDAO      Classe de communication avec la base
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function validateUser ($login, $passwd, $userDAO){
        if(!isset($login) OR empty($login) OR $login=="")
            return Error::$EmptyPseudo;
        if(strlen($login) < 4 or strlen($login) > 16 or !ctype_alnum($login) or is_numeric($login))
            return Error::$SyntaxPseudo;
        if(!isset($passwd) OR empty($passwd) OR $passwd=="" )
            return Error::$EmptyPassword ;
        if(strlen($passwd) < 8 or strlen($passwd) > 50 or !ctype_alnum($passwd))
            return Error::$SyntaxPassword;
        if($userDAO->userExists($login))
            return Error::$ExistingPseudo;
        return Error::$NoError;
    }
}

