<?php
/**
 * Created by Victor.
 * Date: 12/10/2018
 * Time: 16:25
 */

namespace NumberOne\Controller;

include_once __DIR__ . ('/../Database/Database.php');
include_once __DIR__ . ("/../Database/OeuvreDAO.php");
include_once __DIR__ . ("/../Model/Category.php");


use \NumberOne\Model\Category;
use \NumberOne\Database\OeuvreDAO;
use \NumberOne\Database\CategoryDAO;

class EnumPeriod {
    public static $week = "7D"; // 7 jours
    public static $month = "1M"; // 1 mois
    public static $year = "1Y"; // 1 an
    public static $ever = "100Y"; // Toutes les oeuvres
}

// Classe de classement des oeuvres.

class RankOeuvre
{
    /**
     * Retourne une collection du top 3 des oeuvres d'une catégorie dans une période donnée
     * @param Category $category    Le pseudo de l'utilisateur.
     * @param string $period        Le temps d'ancienneté de l'oeuvre
     * @param OeuvreDAO $oeuvreDAO  Classe de communication avec la base
     * @param CategoryDAO $catDAO   Classe de communication avec la base
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function getTop3Oeuvre(Category $category, $period, $oeuvreDao, $catDao) {
        $oeuvres = RankOeuvre::getAllOeuvre($category, $period, $oeuvreDao, $catDao);
        if (is_string($oeuvres))
            return $oeuvres;
        return array_slice($oeuvres, 0, 3);
    }

    /**
     * Retourne une collection complète des oeuvres d'une catégorie dans une période donnée
     * @param Category $category    Le pseudo de l'utilisateur.
     * @param string $period        Le temps d'ancienneté de l'oeuvre
     * @param OeuvreDAO $oeuvreDAO  Classe de communication avec la base
     * @param CategoryDAO $catDAO   Classe de communication avec la base
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function getAllOeuvre(Category $category, $period, $oeuvreDao, $catDao) {
        if (!$catDao->categoryExists($category->getName()))
            return Error::$UndefinedCategory;
        $today = new \DateTime();
        $since = $today->sub(new \DateInterval("P".$period));
        if($since == false)
            return Error::$UnvalidDate;
        $since->format("Y-m-d");
        $oeuvres = $oeuvreDao->getOeuvres($category, $since);
        return $oeuvres;
    }
}