<?php
/**
 * Created by IntelliJ IDEA.
 * User: kristopher
 * Date: 16/10/18
 * Time: 16:53
 */

namespace NumberOne\Controller;

include_once __DIR__ . ('/../Database/Database.php');
include_once __DIR__ . ("/../Database/OeuvreDAO.php");
include_once __DIR__ . ("/../Database/CategoryDAO.php");
include_once __DIR__ . ("/../Database/UserDAO.php");
include_once __DIR__ . ("/../Model/Oeuvre.php");
include_once __DIR__ . ("/../Model/Category.php");
include_once __DIR__ . ("/../Model/User.php");



use \NumberOne\Model\Oeuvre;
use \NumberOne\Model\Category;
use \NumberOne\Model\User;


class CategoryController
{
    public static function submit($categoryName, $dao)
    {

        $cat = new Category($categoryName);

        if ($dao->insertCategory($cat))
            return Error::$NoError;
        return Error::$SumbittingCategoryError;
    }

    function deleteCategory($name, $dao){

        if ($dao->deleteCategory($name))
            return Error::$NoError;
        return Error::$ErrorDeleteCate;

    }
}