<?php

namespace NumberOne\Controller;

use NumberOne\Database\UserDAO;

include_once __DIR__ . ("/../Database/Database.php");
include_once __DIR__ . ("/../Database/UserDAO.php");
include_once __DIR__ . ("/../Model/User.php");
include_once __DIR__ . ("/Error.php");

class AuthentificationController {

    /**
     * Authentification d'un utilisateur.
     * @param UserDAO $userDAO      Classe de communication avec la base
     * @param string $pseudo        Le pseudo de l'utilisateur.
     * @param string $pass          Le mot de passe de l'utilisateur (en clair)
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function Authentification($userDAO,$pseudo ,$pass) {

    $user = $userDAO->getUserFromPseudo($pseudo);
    $errorMessage=AuthentificationController::verifyAuthentification ($pseudo,$pass, $user);

   
    if ( $errorMessage==Error::$NoError){

        $userPassword = $user->getHashPassword();
         if (!password_verify($pass,$userPassword))
             return Error::$AuthFailed;
         $_SESSION['pseudo'] = $pseudo;
         $role=$user->getRole();
         $_SESSION['role'] = $role;
         return Error::$NoError ;
     }
    else
        return $errorMessage;

    }

    /**
     * Vérifie les données d'authentification
     * @param string $pseudo        Le pseudo de l'utilisateur.
     * @param string $pass          Le mot de passe de l'utilisateur (en clair)
     * @param User $user            L'utilisateur connecté
     * @return string               Retourne "OK" en cas de succès, un message d'erreur sinon.
     */
    public static function verifyAuthentification ($login, $passwd, $user){

      // Verification de la validité de l'inscription
      // Verification du pseudo

      if( !isset($login) OR empty($login) OR $login=="")
          return Error::$EmptyPseudo;

      else if (!isset($passwd) OR empty($passwd) OR $passwd=="" )
          return Error::$EmptyPassword;
      
      else if ($user == null)
           return Error::$AuthFailed;

       return Error::$NoError;
    }

}