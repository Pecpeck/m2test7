<?php
namespace NumberOne\Controller ;
/**
 * Created by Victor.
 * Date: 12/10/2018
 * Time: 13:37
 */

include_once ("AuthentificationController.php");
include_once ("InscriptionController.php");
include_once ("OeuvreController.php");
include_once ("RankOeuvre.php");
include_once ("CategoryController.php");
include_once ("UserController.php");
include_once ("../Database/Database.php");
include_once ("../Controller/VoteController.php");
include_once ("../Database/UserDAO.php");
include_once ("../Database/OeuvreDAO.php");
include_once ("../Model/User.php");
include_once ("../Database/CategoryDAO.php");
include_once ("../Model/Category.php");
include_once ("../Database/VoteDAO.php");
include_once ("../Model/Vote.php");
include_once ("../Vue/php/display.php");

use NumberOne\Model\Category;
use \NumberOne\Model\User;
use \NumberOne\Database\Database;
use \NumberOne\Database\UserDAO;
use \NumberOne\Database\OeuvreDAO;
use \NumberOne\Database\CategoryDAO;
use \NumberOne\Database\VoteDAO;
use \NumberOne\Model\Vote;


// Cette page est appellée par la vue. Elle instancie les différentes classes nécessaires et demande le traitement au controlleur.
// Ainsi, les traitements du controlleur sont testables et les outils, mockables.
session_start();
$db = Database::getInstance();

$returnMessage = '';
switch ($_POST['use']) {
    case "inscription" :
        $userDAO = new UserDAO($db);
        $returnMessage = InscriptionController::createUser($userDAO, $_POST['username'], $_POST['password']);
        break;
    case "connexion" :
        $userDAO = new UserDAO($db);
        $returnMessage = AuthentificationController::Authentification($userDAO, $_POST['username'], $_POST['password']);
        break;
    case "add_oeuvre" :
        $dao = new OeuvreDAO($db);
        if (isset($_SESSION['pseudo']))
            $user = new User($_SESSION['pseudo'], '', $_SESSION['role']);
        else
            $user = null;
        $returnMessage = OeuvreController::submit($_POST, $user, $dao);
        break;
    case "get_bar" :
        echo navbarSet();
        break;
    case "add_category" :
        $dao = new CategoryDAO($db);
        $categoryName = $_POST['name'];
        $returnMessage = CategoryController::submit($categoryName, $dao);
        break;
    case "delete_user" :
        $dao = new UserDAO($db);
        $userPseudo = $_POST['pseudo'];
        $returnMessage = UserController::deleteUser($userPseudo, $dao);
        break;
    case "delete_category" :
        $dao = new CategoryDAO($db);
        $catName = $_POST['name'];
        $returnMessage = CategoryController::deleteCategory($catName, $dao);
        break;
    case "get_category" :
        $c = new CategoryDAO($db);
        $catsLibs = $c->getAllCategories();
        $res = array();
        foreach ($catsLibs as $cat) {
            array_push($res, $cat->getName());
        }
        $returnMessage = json_encode($res);
        break;
    case "get_category_admin" :
        $c = new CategoryDAO($db);
        $catsLibs = $c->getAllCategories();
        $res = array();
        foreach ($catsLibs as $cat) {
            array_push($res, $cat->getName());
        }
        $returnMessage = json_encode($res);
        break;
    case "get_oeuvre" :
        $c = new OeuvreDAO($db);
        $oeuvreList = $c->getAllOeuvres();
        $res = array();
        foreach ($oeuvreList as $oeuvre) {
            $tmp = array();
            array_push($tmp, $oeuvre->getTitle());
            array_push($tmp, $oeuvre->getLink());
            array_push($tmp, $oeuvre->getDescription());
            array_push($tmp, $oeuvre->getID());
            array_push($res, $tmp);
        }
        $returnMessage = json_encode($res);
        break;
    case "get_oeuvre_hebdo" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $catList = $cDao->getAllCategories();
        $res = array();
        foreach ($catList as $cat){
            $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$week, $oDao, $cDao);
            foreach ($oeuvreList as $oeuvre) {
                $tmp = array();
                array_push($tmp, $oeuvre->getTitle());
                array_push($tmp, $oeuvre->getLink());
                array_push($tmp, $oeuvre->getDescription());
                array_push($tmp, $oeuvre->getID());
                array_push($res, $tmp);
            }
        }
        $returnMessage = json_encode($res);
        break;
    case "get_oeuvre_mensu" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $catList = $cDao->getAllCategories();
        $preRes = array();
        $res = array();
        foreach ($catList as $cat){
            $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$month, $oDao, $cDao);
            foreach ($oeuvreList as $oeuvre) {
                $tmp = array();
                array_push($tmp, $oeuvre->getTitle());
                array_push($tmp, $oeuvre->getLink());
                array_push($tmp, $oeuvre->getDescription());
                array_push($tmp, $oeuvre->getID());
                array_push($res, $tmp);
            }
        }
        $returnMessage = json_encode($res);
        break;
    case "get_oeuvre_annuel" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $catList = $cDao->getAllCategories();
        $preRes = array();
        $res = array();
        foreach ($catList as $cat){
            $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$year, $oDao, $cDao);
            foreach ($oeuvreList as $oeuvre) {
                $tmp = array();
                array_push($tmp, $oeuvre->getTitle());
                array_push($tmp, $oeuvre->getLink());
                array_push($tmp, $oeuvre->getDescription());
                array_push($tmp, $oeuvre->getID());
                array_push($res, $tmp);
            }
        }
        $returnMessage = json_encode($res);
        break;
    case "get_all_user" :
        $u = new UserDAO($db);
        $userList = $u->getAllUsers();
        $res = array();
        foreach ($userList as $user) {
            $tmp = array();
            array_push($tmp, $user->getPseudo());
            array_push($tmp, $user->getRole());
            array_push($res, $tmp);
        }
        $returnMessage = json_encode($res);
        break;
    case "get_oeuvre_solo" :
        $c = new OeuvreDAO($db);
        $oeuvre = $c->getOeuvreFromId($_POST["id"]);
        $res = array();

        array_push($res, $oeuvre->getTitle());
        array_push($res, $oeuvre->getLink());
        array_push($res, $oeuvre->getDescription());
        array_push($res, $oeuvre->getCategory()->getName());
        array_push($res, $oeuvre->getDate()->format('d-m-Y'));
        array_push($res, $oeuvre->getID());
        $returnMessage = json_encode($res);
        break;
    case "vote" :
        if (isset($_SESSION['pseudo'])){
            $user = new User($_SESSION['pseudo'], '', $_SESSION['role']);
            $dao = new VoteDAO($db);
            $c = new OeuvreDAO($db);
            $oeuvre = $c->getOeuvreFromId($_POST['id']);
            $note = $_POST["vote_value"];
            $vote = new Vote($user, $oeuvre, $note);
            $returnMessage = VoteController::SubmitVote($vote, $dao);
        }
        else
            $returnMessage = Error::$NotConnectedUser;

        break;
    case "get_vote" :
        $dao = new VoteDAO($db);
        $c = new OeuvreDAO($db);
        $oeuvre = $c->getOeuvreFromId($_POST["id"]);
        if (isset($_SESSION['pseudo'])) {
            $user = new User($_SESSION['pseudo'], '', $_SESSION['role']);
            $userDao = new UserDAO($db);
            $userVoteValue = VoteController::getVoteValue($user, $oeuvre, $dao, $userDao);
        }
        else
            $userVoteValue = 0;
        $like = VoteController::getNotes($oeuvre, $dao, 1);
        $dislike = VoteController::getNotes($oeuvre, $dao, -1);
        $args = [$like, $dislike, $userVoteValue];
        $returnMessage = json_encode($args);
        break;
    case "filtre_oeuvre" :
        $c = new OeuvreDAO($db);
        $oeuvreList = $c->getAllOeuvres();
        $res = array();
        foreach ($oeuvreList as $oeuvre){
            if($oeuvre->getCategory()->getName() == $_POST['category']) {
                $tmp = array();
                array_push($tmp, $oeuvre->getTitle());
                array_push($tmp, $oeuvre->getLink());
                array_push($tmp, $oeuvre->getDescription());
                array_push($tmp, $oeuvre->getID());
                array_push($res, $tmp);
            }
        }
        $returnMessage = json_encode($res);
        break;
    case "filtre_oeuvre_hebdo" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $cat = new Category($_POST['category']);
        $res = array();
        $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$week, $oDao, $cDao);
        foreach ($oeuvreList as $oeuvre) {
            $tmp = array();
            array_push($tmp, $oeuvre->getTitle());
            array_push($tmp, $oeuvre->getLink());
            array_push($tmp, $oeuvre->getDescription());
            array_push($tmp, $oeuvre->getID());
            array_push($res, $tmp);
        }
        $returnMessage = json_encode($res);
        break;
    case "filtre_oeuvre_mensu" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $cat = new Category($_POST['category']);
        $res = array();
        $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$month, $oDao, $cDao);
        foreach ($oeuvreList as $oeuvre) {
            $tmp = array();
            array_push($tmp, $oeuvre->getTitle());
            array_push($tmp, $oeuvre->getLink());
            array_push($tmp, $oeuvre->getDescription());
            array_push($tmp, $oeuvre->getID());
            array_push($res, $tmp);
        }
        $returnMessage = json_encode($res);
        break;
    case "filtre_oeuvre_annuel" :
        $cDao = new CategoryDAO($db);
        $oDao = new OeuvreDAO($db);
        $cat = new Category($_POST['category']);
        $res = array();
        $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, EnumPeriod::$year, $oDao, $cDao);
        foreach ($oeuvreList as $oeuvre) {
            $tmp = array();
            array_push($tmp, $oeuvre->getTitle());
            array_push($tmp, $oeuvre->getLink());
            array_push($tmp, $oeuvre->getDescription());
            array_push($tmp, $oeuvre->getID());
            array_push($res, $tmp);
        }
        $returnMessage = json_encode($res);
        break;
    case "get_top3_sorted_oeuvre" :
        $oDao = new OeuvreDAO($db);
        $cDao = new CategoryDAO($db);
        $cat = $_POST['category'];
        switch ($_POST['date']) {
            case "week" : $period = EnumPeriod::$week; break;
            case "month" : $period = EnumPeriod::$month; break;
            case "year" : $period = EnumPeriod::$year; break;
            default : $period = EnumPeriod::$ever;
        }
        $oeuvreList = RankOeuvre::getTop3Oeuvre($cat, $period, $oDao, $cDao);
        $returnMessage = json_encode($oeuvreList);
        break;
    case "get_all_sorted_oeuvre" :
        $oDao = new OeuvreDAO($db);
        $cDao = new CategoryDAO($db);
        $cat = $_POST['category'];
        switch ($_POST['date']) {
            case "week" : $period = EnumPeriod::$week; break;
            case "month" : $period = EnumPeriod::$month; break;
            case "year" : $period = EnumPeriod::$year; break;
            default : $period = EnumPeriod::$ever;
        }
        $oeuvreList = RankOeuvre::getAllOeuvre($cat, $period, $oDao, $cDao);
        $returnMessage = json_encode($oeuvreList);
        break;
}

echo $returnMessage;
exit();