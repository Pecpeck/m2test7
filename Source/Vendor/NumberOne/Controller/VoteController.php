<?php
namespace NumberOne\Controller;

include_once __DIR__ . ('/../Database/Database.php');
include_once __DIR__ . ("/../Database/VoteDAO.php");

include_once __DIR__ . ("/../Model/Oeuvre.php");
include_once __DIR__ . ("/../Model/Vote.php");
include_once __DIR__ . ("/../Model/User.php");


use \NumberOne\Model\Oeuvre;
use \NumberOne\Model\Vote;
use \NumberOne\Model\User;

class VoteController {

    public static function SubmitVote(Vote $vote, $dao) {
        $user = $vote->getUser();
        $note = $vote->getNote();
        if (empty($user))
            return Error::$NotConnectedUser;
        if ($note != 1 and $note != 0 and $note != -1)
            return Error::$ErrorNote;
        if ($dao->insertVote($vote))
            return Error::$NoError;
        return Error::$SumbittingVoteError;
    }

    public static function getVoteValue(User $user, Oeuvre $oeuvre, $dao, $userDao) {
        $userId = $userDao->getIdFromPseudo($user->getPseudo());
        $oeuvreId = $oeuvre->getID();
        if ($res = $dao->getVoteValue($userId, $oeuvreId))
            return $res;
        return 0; // L'utilisateur n'a jamais voté sur cette oeuvre
    }

    public static function getNotes(Oeuvre $oeuvre, $dao, $note) {
        $oeuvreId = $oeuvre->getID();
        if ($note == 1) {
            $res = $dao->getLikesOeuvres($oeuvreId);
            if (is_numeric($res))
                return $res;
            else
                return false;
        }
        if ($note == -1) {
            $res = $dao->getDislikesOeuvres($oeuvreId);
            if (is_numeric($res))
                return $res;
            else
                return false;
        }
        if($note == 0){
            return 0;
        }
        return Error::$ErrorNote;
    }

}