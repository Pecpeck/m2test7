<?php
/**
 * Created by Victor
 * Date: 02/10/2018
 * Time: 11:46
 */

namespace NumberOne\Model;

class Vote
{
    private $user;
    private $oeuvre;
    private $note;



    public function __construct(User $user, Oeuvre $oeuvre, $note)
    {
        $this->user = $user;
        $this->oeuvre = $oeuvre;
        $this->note =  $note;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getOeuvre()
    {
        return $this->oeuvre;
    }

    public function getNote()
    {
        return $this->note;
    }
    
}