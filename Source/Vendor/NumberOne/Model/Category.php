<?php

/**
 * Created by Victor.
 * Date: 28/09/2018
 * Time: 15:45
 */

namespace NumberOne\Model;

class Category
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}