<?php

namespace NumberOne\Model;
/**
 * Created by Victor.
 * Date: 28/09/2018
 * Time: 15:36
 */

namespace NumberOne\Model;

class EnumRole {
    public static $User = "user";
    public static $Admin = "admin";
}

class User
{
    private $pseudo;
    private $hashPassword;
    private $role;

    public function __construct($pseudo, $hashPassword, $role)
    {
        $this->pseudo = $pseudo;
        $this->hashPassword = $hashPassword;
        $this->role = ($role == EnumRole::$Admin)? EnumRole::$Admin : EnumRole::$User;
    }

    public function getHashPassword()
    {
        return $this->hashPassword;
    }

    public function getPseudo()
    {
        return $this->pseudo;
    }

    public function getRole()
    {
        return $this->role;
    }
}