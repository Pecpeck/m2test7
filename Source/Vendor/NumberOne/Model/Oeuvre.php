<?php
/**
 * Created by Victor.
 * Date: 28/09/2018
 * Time: 15:27
 */

namespace NumberOne\Model;

class Oeuvre {
    private $id;
    private $title;
    private $description;
    private $link;
    private $category;
    private $user;
    private $date;

    public function __construct($title, $description, $link, Category $category, User $user, \DateTime $date) {
        $this->title = $title;
        $this->description = $description;
        $this->link = $link;
        $this->category = $category;
        $this->user = $user;
        $this->date = $date;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getDate()
    {
        return $this->date;
    }
}
