<?php

namespace NumberOne\Model;

/**
 * Created by Victor.
 * Date: 02/10/2018
 * Time: 11:43
 */


class Comment
{
    private $id;
    private $oeuvre;
    private $text;
    private $user;

    public function __construct($text, Oeuvre $oeuvre, User $user)
    {
        $this->oeuvre = $oeuvre;
        $this->text = $text;
        $this->user = $user;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getOeuvre()
    {
        return $this->oeuvre;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getUser()
    {
        return $this->user;
    }
}