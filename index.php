<?php
session_start();
require_once 'Source/Vendor/NumberOne/Vue/php/display.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--<link href="/Source/Vendor/NumberOne/Vue/css/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2" rel="stylesheet">-->
      <!--Import materialize.css-->
      <!--<link type="text/css" rel="stylesheet" href="/Source/Vendor/NumberOne/Vue/css/materialize.min.css"  media="screen,projection"/>-->
      <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
<body>
  <!-- Barre de navigation/Bouton ajout oeuvre-->
  <div id="navbar">
    <?php navbarSet() ?>
  </div>
  <!-- Modal Structure Connexion -->
  <div id="modal1" class="modal">
      <div class="modal-content">
          <h4>Connexion</h4>
          <div class="row">
              <form id="form_connex" method="post" class="col s6">
                  <div class="row">
                      <div class="input-field col s12">
                          <input  id="pseudoConnexion" type="text" class="validate">
                          <label for="pseudoConnexion">Pseudo</label>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s12">
                          <input id="passwordConnexion" type="password" class="validate">
                          <label for="passwordConnexion">Password</label>
                      </div>
                  </div>
                  <button id="submit" class="right blue btn waves-effect waves-light" type="submit">Submit
                      <i class="material-icons right">send</i>
                  </button>
              </form>
          </div>
      </div>
  </div>

  <div id="modal2" class="modal">
      <div class="modal-content">
          <h4>Inscription</h4>
          <div class="row">
              <form id="form_inscrip" method="post" class="col s6">
                  <div class="row">
                      <div class="input-field col s12">
                          <input id="pseudoInscription" type="text" class="validate">
                          <label for="pseudoInscription">Pseudo</label>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s12">
                          <input id="passwordInscription" type="password" class="validate">
                          <label for="passwordInscription">Password</label>
                      </div>
                  </div>
                  <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Submit
                      <i class="material-icons right">send</i>
                  </button>
              </form>
          </div>
      </div>
  </div>

  <div id="modalO" class="modal">
      <div class="modal-content">
          <h4>Ajouter un oeuvre</h4>
          <div class="row">
              <form id="form_add" method="post" action="#" class="col s6">
                  <div class="row">
                      <div class="input-field col s12">
                          <input  id="titre" type="text" class="validate">
                          <label for="titre">Titre de l'oeuvre</label>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s12">
                          <input id="lien" type="text" class="validate">
                          <label for="lien">Lien de l'oeuvre</label>
                      </div>
                  </div>
                  <div class="row">
                      <label for="filtre-categorie">Catégorie</label>
                      <div class="input-field col s12 m12">
                          <select id="categorie" class=".selectCat" name="filtre-categorie" style="display:block;">
                              <option value="" disabled selected>Choisissez votre catégorie</option>
                          </select>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col s12">
                          <textarea id="description" class="materialize-textarea"></textarea>
                          <label for="description">Description</label>
                      </div>
                  </div>
                  <button href="#" class="right blue btn waves-effect waves-light" type="submit" name="action">Ajouter
                      <i class="material-icons right">add</i>
                  </button>
              </form>
          </div>
      </div>
  </div>

  <ul id="tabs-swipe-demo" class="tabs green">
      <li class="tab col s3"><a class="active white-text" href="#allOeuvre">Toutes les oeuvres</a></li>
      <li class="tab col s3"><a class="white-text" href="#hebdoOeuvre">Hebdomadaire</a></li>
      <li class="tab col s3"><a class="white-text" href="#mensuelOeuvre">Mensuel</a></li>
      <li class="tab col s3"><a class="white-text" href="#annuelOeuvre">Anuelle</a></li>
  </ul>
  <div id="allOeuvre" class="col s12">
      <div>
          <form id="form_filtre" method="post" action="#" class="col s6">
              <label for="categorieAll">Filtrer les oeuvres</label>
              <select id="categorieAll" class=".selectCat" name="filtre-categorie" style="display:block;">
                  <option value="" disabled selected>Filtrer par catégorie</option>
              </select>
              <button href="#" class="left blue btn waves-effect waves-light" type="submit" name="action">Filtrer
                  <i class="material-icons right">done</i>
              </button>
              <a href="#" onclick="resetOeuvre()" id="reset" class="red waves-effect waves-light btn"><i class="material-icons right">replay</i>Reset</a>
          </form>
      </div>
      <br>
      <div id="oeuvreDiv">
          <ul id="listOeuvre" class="collection"></ul>
      </div>
  </div>

  <div id="hebdoOeuvre" class="col s12">
      <div>
          <form id="form_filtre_hebdo" method="post" action="#" class="col s6">
              <label for="categorieAllHebdo">Filtrer les oeuvres</label>
              <select id="categorieAllHebdo" class=".selectCat" name="filtre-categorie" style="display:block;">
                  <option value="" disabled selected>Filtrer par catégorie</option>
              </select>
              <button href="#" class="left blue btn waves-effect waves-light" type="submit" name="action">Filtrer
                  <i class="material-icons right">done</i>
              </button>
              <a href="#" onclick="resetOeuvreHebdo()" id="resetHebdo" class="red waves-effect waves-light btn"><i class="material-icons right">replay</i>Reset</a>
          </form>
      </div>
      <div id="oeuvreDivHebdo">
          <ul id="listOeuvreHebdo" class="collection"></ul>
      </div>
  </div>


  <div id="mensuelOeuvre" class="col s12">
      <div>
          <form id="form_filtre_mensu" method="post" action="#" class="col s6">
              <label for="categorieAllMensu">Filtrer les oeuvres</label>
              <select id="categorieAllMensu" class=".selectCat" name="filtre-categorie" style="display:block;">
                  <option value="" disabled selected>Filtrer par catégorie</option>
              </select>
              <button href="#" class="left blue btn waves-effect waves-light" type="submit" name="action">Filtrer
                  <i class="material-icons right">done</i>
              </button>
              <a href="#" onclick="resetOeuvreMensu()" id="resetMensu" class="red waves-effect waves-light btn"><i class="material-icons right">replay</i>Reset</a>
          </form>
      </div>
      <div id="oeuvreDivMensu">
          <ul id="listOeuvreMensu" class="collection"></ul>
      </div>
  </div>

  <div id="annuelOeuvre" class="col s12">
      <div>
          <form id="form_filtre_annuel" method="post" action="#" class="col s6">
              <label for="categorieAllAnnuel">Filtrer les oeuvres</label>
              <select id="categorieAllAnnuel" class=".selectCat" name="filtre-categorie" style="display:block;">
                  <option value="" disabled selected>Filtrer par catégorie</option>
              </select>
              <button href="#" class="left blue btn waves-effect waves-light" type="submit" name="action">Filtrer
                  <i class="material-icons right">done</i>
              </button>
              <a href="#" onclick="resetOeuvreAnnuel()" id="resetAnnuel" class="red waves-effect waves-light btn"><i class="material-icons right">replay</i>Reset</a>
          </form>
      </div>
      <div id="oeuvreDivAnnuel">
          <ul id="listOeuvreAnnuel" class="collection"></ul>
      </div>
  </div>


  <!--<script src="/Source/Vendor/NumberOne/Vue/js/jquery.min.js"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--<script src="/Source/Vendor/NumberOne/Vue/js/materialize.min.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script type="text/javascript" src="/~m2test7/prod/Source/Vendor/NumberOne/Vue/js/fonction.js"></script>
  </body>
</html>