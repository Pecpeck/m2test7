<?php
/**
 * Created by PhpStorm.
 * User: mnauval
 * Date: 04/10/18
 * Time: 16.34
 */

namespace NumberOne\Database\tests\units;
use \atoum;
use NumberOne\Model\EnumRole;

class UserDAO extends atoum
{
    public function testUserExists()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetchColumn = function()
        {
            return 3;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->boolean($userDAO->userExists("test"))->isEqualTo(true);
    }

    public function testUserDoesntExist()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetchColumn = function()
        {
            return 0;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->boolean($userDAO->userExists("test"))->isEqualTo(false);
    }

    public function testGetIdFromPseudo()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "id" => 1
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->integer($userDAO->getIdFromPseudo("test"))->isEqualTo(1);
    }

    public function testGetIdFromPseudoNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($userDAO->getIdFromPseudo("test"))->isEqualTo(null);
    }

    public function testGetPasswordFromPseudo()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "motDePasse" => "txkjereji234efc!"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->string($userDAO->getPasswordFromPseudo("test"))->isEqualTo("txkjereji234efc!");
    }

    public function testGetPasswordFromPseudoNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($userDAO->getPasswordFromPseudo("test"))->isEqualTo(null);
    }

    public function testGetUserFromPseudo()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        $expectedResult = new \NumberOne\Model\User("victorm", "ezirihfidsqjfieir$*£",
                            \NumberOne\Model\EnumRole::$User);
        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "id" => 1,
                "pseudo" => "victorm",
                "motDePasse" => "ezirihfidsqjfieir$*£",
                "role" => "user"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->object($userDAO->getUserFromPseudo("victorm"))->isEqualTo($expectedResult);
    }

    public function testGetUserFromPseudoNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($userDAO->getUserFromPseudo("victorm"))->isEqualTo(null);
    }

    public function testGetUserFromId()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        $expectedResult = new \NumberOne\Model\User("mnauval", "ezirihfidsqjfieir$*£",
            \NumberOne\Model\EnumRole::$User);
        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "id" => 2,
                "pseudo" => "mnauval",
                "motDePasse" => "ezirihfidsqjfieir$*£",
                "role" => "user"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->object($userDAO->getUserFromId(1))->isEqualTo($expectedResult);
    }

    public function testGetUserFromIdNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($userDAO->getUserFromId("victorm"))->isEqualTo(null);
    }

    public function testGetAllUsers()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        $expectedResult = array(
            new \NumberOne\Model\User("mnauval", "1234", \NumberOne\Model\EnumRole::$Admin),
            new \NumberOne\Model\User("vmenier", "1234", \NumberOne\Model\EnumRole::$User)
        );
        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array(
                array(
                    "id" => 1,
                    "pseudo" => "mnauval",
                    "motDePasse" => "1234",
                    "role" => "admin"
                ),
                array(
                    "id" => 1,
                    "pseudo" => "vmenier",
                    "motDePasse" => "1234",
                    "role" => "user"
                )
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($userDAO->getAllUsers())->isEqualTo($expectedResult);
    }

    public function testGetAllUsersEmpty()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $userDAO = new \NumberOne\Database\UserDAO($mockDB);

        $expectedResult = array();
        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array();
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($userDAO->getAllUsers())->isEqualTo($expectedResult);
    }
}