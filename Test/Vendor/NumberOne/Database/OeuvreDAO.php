<?php
/**
 * Created by mnauval.
 * Date: 04/10/18
 * Time: 18.50
 */

namespace NumberOne\Database\tests\units;

use \atoum;

use NumberOne\Model\Category;
use NumberOne\Model\EnumRole;
use NumberOne\Model\Oeuvre;
use NumberOne\Model\User;

class OeuvreDAO extends atoum
{
    public function TestGetOeuvreFromId()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $oeuvreDAO = new \NumberOne\Database\OeuvreDAO($mockDB);
        $expectedCategory = new Category("peinture");
        $expectedUser = new User("mnauval", "1234", EnumRole::$User);
        $expectedOeuvre = new Oeuvre("un titre", "une description", "un lien", $expectedCategory,
                            $expectedUser, new \DateTime("2018-05-05"));
        $expectedOeuvre->setID(1);

        if ($this->calling($mockPDOStatement)->fetch[1] = function()
        { 
        return array(
                "id" => 1,
                "idUtilisateur" => 1,
                "titre" => "un titre",
                "description" => "une description",
                "idCategorie" => 1,
                "lien" => "un lien",
                "date" =>new \DateTime("2018-05-05")
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[2] = function()
        {
            return array(
                "id" => 1,
                "pseudo" => "mnauval",
                "motDePasse" => "1234",
                "role" => "user"
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[3] = function()
        {
            return array(
                "libelle" => "peinture"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->object($oeuvreDAO->getOeuvreFromId(1))->isEqualTo($expectedOeuvre);
    }

   public function testGetOeuvreFromIdNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $oeuvreDAO = new \NumberOne\Database\OeuvreDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function(){
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($oeuvreDAO->getOeuvreFromId(1))->isEqualTo(null);
    }

    public function testGetAllOeuvres()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $oeuvreDAO = new \NumberOne\Database\OeuvreDAO($mockDB);

        $oeuvre1 = new Oeuvre("un titre",
            "une description",
            "un lien",
            new Category("peinture"),
            new User("mnauval", "1234", EnumRole::$User),new \DateTime("2018-05-05"));
        $oeuvre1->setID(1);

        $oeuvre2 = new Oeuvre("un titre 2",
            "une description 2",
            "un lien 2",
            new Category("photo"),
            new User("vmenier", "5678", EnumRole::$User),new \DateTime("2018-05-05"));
        $oeuvre2->setID(2);

        $expected = array(
            $oeuvre1,
            $oeuvre2
        );

        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array(
                array(
                    "id" => 1,
                    "idUtilisateur" => 1,
                    "titre" => "un titre",
                    "description" => "une description",
                    "idCategorie" => 1,
                    "lien" => "un lien",
                    "date" => new \DateTime("2018-05-05")
                ),
                array(
                    "id" => 2,
                    "idUtilisateur" => 12,
                    "titre" => "un titre 2",
                    "description" => "une description 2",
                    "idCategorie" => 2,
                    "lien" => "un lien 2",
                    "date" => new \DateTime("2018-05-05")
                )
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[1] = function()
        {
            return array(
                "id" => 1,
                "pseudo" => "mnauval",
                "motDePasse" => "1234",
                "role" => "user"
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[2] = function()
        {
            return array(
                "id" => 1,
                "libelle" => "peinture"
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[3] = function()
        {
            return array(
                "id" => 12,
                "pseudo" => "vmenier",
                "motDePasse" => "5678",
                "role" => "user"
            );
        });
        if ($this->calling($mockPDOStatement)->fetch[4] = function()
        {
            return array(
                "id" => 2,
                "libelle" => "photo"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($oeuvreDAO->getAllOeuvres())->isEqualTo($expected);
    }
    

    public function testGetAllOeuvresNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $oeuvreDAO = new \NumberOne\Database\OeuvreDAO($mockDB);

        $expected = array();
        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array();
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($oeuvreDAO->getAllOeuvres())->isEqualTo($expected);
    }
    
}