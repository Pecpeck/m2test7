<?php

namespace NumberOne\Database\tests\units;

use \atoum;
use NumberOne\Model\Category;
use NumberOne\Model\Oeuvre;

class CategoryDAO extends atoum
{

    public function testCategoryDoesntExist()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetchColumn = function()
        {
                return 0;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->boolean($categoryDAO->categoryExists(""))->isEqualTo(false);
    }

    public function testCategoryExists()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetchColumn = function()
        {
            return 1;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->boolean($categoryDAO->categoryExists(""))->isEqualTo(true);
    }

    public function testGetLibelleFromId()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "libelle" => "test"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->string($categoryDAO->getLibelleFromId(1))->isEqualTo("test");
    }

    public function testGetLibelleFromIdNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($categoryDAO->getLibelleFromId(1))->isEqualTo(null);
    }

    public function testGetIdFromLibelle()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
           return array(
               "id" => 1
           );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->integer($categoryDAO->getIdFromLibelle(""))->isEqualTo(1);
    }

    public function testGetIdFromLibelleNonExisting()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($categoryDAO->getIdFromLibelle(""))->isEqualTo(null);
    }

    public function testGetAllCategories()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberONe\Database\CategoryDAO($mockDB);
        $expectedResult = array(
            new Category("test1"), new Category("test2")
        );

        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array(
                array(
                    "id" => 1,
                    "libelle" => "test1"
                ),
                array(
                    "id" => 2,
                    "libelle" => "test2"
                )
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($categoryDAO->getAllCategories())->isEqualTo($expectedResult);
    }

    public function testGetAllCategoriesEmpty()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberONe\Database\CategoryDAO($mockDB);
        $expectedResult = array();

        if ($this->calling($mockPDOStatement)->fetchAll = function()
        {
            return array();
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->array($categoryDAO->getAllCategories())->isEqualTo($expectedResult);
    }

    public function testGetCategoryFromId()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);
        $expectedResult = new Category("test");

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return array(
                "libelle" => "test"
            );
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->object($categoryDAO->getCategoryFromId(1))->isEqualTo($expectedResult);
    }

    public function testGetCategoryFromIdEmpty()
    {
        $mockDB = new \Mock\Database();
        $mockPDOStatement = new \Mock\PDOStatement();
        $categoryDAO = new \NumberOne\Database\CategoryDAO($mockDB);

        if ($this->calling($mockPDOStatement)->fetch = function()
        {
            return false;
        });
        if ($this->calling($mockDB)->run = $mockPDOStatement);
        $this->variable($categoryDAO->getCategoryFromId(1))->isEqualTo(null);
    }
}


