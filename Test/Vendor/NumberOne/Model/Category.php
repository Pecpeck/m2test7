<?php
/**
 * Created by Victor.
 * Date: 02/10/2018
 * Time: 15:37
 */

namespace NumberOne\Model\test\units;
use \atoum;

class Category extends atoum
{
    public function testConstruct()
    {
        $x = "Toto";

        $this
            ->if($category = new \NumberOne\Model\Category($x))
            ->then
                ->string($category->getName())
                    ->isEqualTo($x)
        ;
    }
}
?>