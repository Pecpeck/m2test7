<?php
/**
 * Created by Victor.
 * Date: 02/10/2018
 * Time: 16:10
 */

namespace NumberOne\Model\tests\units;
use \atoum;

class Comment extends atoum
{
    public function testConstruct()
    {
        $x = "Toto";
        $cat = new \NumberOne\Model\Category("C");
        $user = new \NumberOne\Model\User("User", "P", \NumberOne\Model\EnumRole::$User);
        $oeuvre = new \NumberOne\Model\Oeuvre("T", "D", "L", $cat, $user, new \DateTime());


        $this
            ->if($comment = new \NumberOne\Model\Comment($x, $oeuvre, $user))
            ->then
                ->string($comment->getText())
                    ->isEqualTo($x)
                ->object($comment->getOeuvre())
                    ->isIdenticalTo($oeuvre)
        ;
    }
}