<?php
/**
 * Created by Victor.
 * Date: 04/10/2018
 * Time: 14:45
 */

namespace NumberOne\Model\tests\units;
use \atoum;
use NumberOne\Model\EnumRole;

class User extends atoum
{
    public function testConstruct()
    {
        $u = "User";
        $p = "PASS";
        $r = EnumRole::$User;

        $this
            ->if($user = new \NumberOne\Model\User($u, $p, $r))
            ->then
                ->string($user->getPseudo())
                    ->isEqualTo($u)
                ->string($user->getHashPassword())
                    ->isEqualTo($p)
                ->string($user->getRole())
                    ->isEqualTo($r)
        ;
    }
}