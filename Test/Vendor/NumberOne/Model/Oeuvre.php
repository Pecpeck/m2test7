<?php
/**
 * Created by Victor.
 * Date: 04/10/2018
 * Time: 14:40
 */

namespace NumberOne\Model\tests\units;
use \atoum;
use NumberOne\Model\EnumNote;
use NumberOne\Model\EnumRole;

class Oeuvre extends atoum
{
    public function testConstruct()
    {
        $t = "T";
        $d = "D";
        $l = "L";
        $cat = new \NumberOne\Model\Category("C");
        $user = new \NumberOne\Model\User("User", "P", EnumRole::$User);
        $date=new \DateTime();
        $this
            ->if($oeuvre = new \NumberOne\Model\Oeuvre($t, $d, $l, $cat, $user,$date))
            ->then
                ->string($oeuvre->getTitle())
                    ->isEqualTo($t)
                ->string($oeuvre->getDescription())
                    ->isEqualTo($d)
                ->string($oeuvre->getLink())
                    ->isEqualTo($l)
                ->object($oeuvre->getCategory())
                    ->isIdenticalTo($cat)
                ->object($oeuvre->getDate())
                    ->isIdenticalTo($date)
        ;
    }
}