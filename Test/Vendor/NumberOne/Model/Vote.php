<?php
/**
 * Created by Victor.
 * Date: 04/10/2018
 * Time: 15:00
 */

namespace NumberOne\Model\tests\units;
use \atoum;
use NumberOne\Model\EnumRole;

class Vote extends atoum
{
    public function testConstruct()
    {
        $user = new \NumberOne\Model\User("User", "PASS", EnumRole::$User);
        $cat = new \NumberOne\Model\Category("C");
        $oeuvre = new \NumberOne\Model\Oeuvre("T", "D", "L", $cat, $user, new \DateTime());
        $note = 5;

        $this
            ->if($vote = new \NumberOne\Model\Vote($user, $oeuvre, $note))
            ->then
                ->object($vote->getUser())
                    ->isIdenticalTo($user)
                ->object($vote->getOeuvre())
                    ->isIdenticalTo($oeuvre)
                ->integer($vote->getNote())
                    ->isEqualTo($note)
        ;
    }
}
