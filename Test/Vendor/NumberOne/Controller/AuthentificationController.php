<?php
/**
 * Created by IntelliJ IDEA.
 * User: kristopher
 * Date: 12/10/18
 * Time: 14:42
 */

namespace NumberOne\Controller\tests\units;
use \atoum;

use NumberOne\Model\EnumRole;
use NumberOne\Controller\Error;



class AuthentificationController extends atoum
{
    
    public function testAuthentificationValide ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($user=new \NumberOne\Model\User('abdel','$2y$10$PnjzoSF17GhoYSMGkkX5PuMjuxdEEtdYkH.bQbFs9OrHPPScGqnjO',EnumRole::$User))
            ->and ($this->calling($userdao)->getUserFromPseudo=$user )
            ->and($myarray = array ( "pseudo"=>"abdel",
                "password"=>"hello"
            )
            )
            ->if($myObject = \NumberOne\Controller\AuthentificationController::Authentification($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$NoError)
        ;
    }

    public function testAuthentificationPseudoVide ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and ($this->calling($userdao)->getUserFromPseudo=false )
            ->and($myarray = array ( "pseudo"=>"",
                "password"=>"mypassword"
            )
            )
            ->if($myObject = \NumberOne\Controller\AuthentificationController::Authentification($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$EmptyPseudo)
        ;
    }

    public function testAuthentificationPasswordVide ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            // on peut jamais trouver un psceudo dont le mot de passe est vide dans la DB
            ->and ($this->calling($userdao)->getUserFromPseudo=false )
            ->and($myarray = array ( "pseudo"=>"abdellah",
                "password"=>""
            )
            )
            ->if($myObject = \NumberOne\Controller\AuthentificationController::Authentification($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$EmptyPassword)
        ;
    }

    public function testAuthentificationPasswordIsFalse( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($user=new \NumberOne\Model\User('abdel','$2y$10$PnjzoSF17GhoYSMGkkX5PuMjuxdEEtdYkH.bQbFs9OrHPPScGqnjO',EnumRole::$User))
            ->and($this->calling($userdao)->userExists=TRUE )
            ->and($myarray = array ( "pseudo"=>"abdel",
                "password"=>"falsePaswword" )
                   )
            ->if($myObject = \NumberOne\Controller\AuthentificationController::Authentification($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$AuthFailed)

        ;
    }

    public function testAuthentificationPseudoIsFalse( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($user=new \NumberOne\Model\User('abdel','$2y$10$PnjzoSF17GhoYSMGkkX5PuMjuxdEEtdYkH.bQbFs9OrHPPScGqnjO',EnumRole::$User))
            ->and($this->calling($userdao)->userExists=false )
            ->and($myarray = array ( "pseudo"=>"NonExistingPseudo",
                "password"=>"falsePaswword" )
                   )
            ->if($myObject = \NumberOne\Controller\AuthentificationController::Authentification($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$AuthFailed)

        ;
    }

}
