<?php
/**
 * Created by Victor.
 * Date: 15/10/2018
 * Time: 12:59
 */

namespace NumberOne\Controller\tests\units;

use \atoum;
use NumberOne\Controller\EnumPeriod;
use NumberOne\Model\Oeuvre;
use NumberOne\Controller\Error;
use NumberOne\Model\Category;
use NumberOne\Model\EnumRole;

class RankOeuvre extends atoum
{
    private function init ()
    {
        $user = new \NumberOne\Model\User("User", "User", EnumRole::$User);
        $category = new Category("A");
        $date = new \DateTime();
        $oeuvres = array(new Oeuvre("title", "desc", "link", $category, $user, $date),
                    new Oeuvre("title2", "desc", "link", $category, $user, $date),
                    new Oeuvre("title3", "desc", "link", $category, $user, $date),
                    new Oeuvre("title4", "desc", "link", $category, $user, $date));
        return $oeuvres;
    }

    public function testTop3Oeuvre()
    {
        $this
            ->given($oeuvres = $this->init())
                ->and($oDao = new \Mock\OeuvreDAO)
                ->and($this->calling($oDao)->getOeuvres = $oeuvres)
                ->and($cDao = new \Mock\CategoryDAO)
                ->and($this->calling($cDao)->categoryExists = true)
            ->if($res = \NumberOne\Controller\RankOeuvre::getTop3Oeuvre(new Category("A"),EnumPeriod::$ever, $oDao, $cDao))
                ->then
                ->integer(count($res))
                ->isEqualTo(3);
    }

    public function testAllOeuvre()
    {
        $this
            ->given($oeuvres = $this->init())
                ->and($oDao = new \Mock\OeuvreDAO)
                ->and($this->calling($oDao)->getOeuvres = $oeuvres)
                ->and($cDao = new \Mock\CategoryDAO)
                ->and($this->calling($cDao)->categoryExists = true)
            ->if($res = \NumberOne\Controller\RankOeuvre::getAllOeuvre(new Category("A"),EnumPeriod::$ever, $oDao, $cDao))
                ->then
                ->integer(count($res))
                ->isEqualTo(4);
    }

    public function testNoOeuvreOnDB()
    {
        $this
            ->given($oeuvres = $this->init())
                ->and($oDao = new \Mock\OeuvreDAO)
                ->and($this->calling($oDao)->getOeuvres = array())
                ->and($cDao = new \Mock\CategoryDAO)
                ->and($this->calling($cDao)->categoryExists = true)
            ->if($res = \NumberOne\Controller\RankOeuvre::getAllOeuvre(new Category("A"),EnumPeriod::$ever, $oDao, $cDao))
                ->then
                ->integer(count($res))
                ->isEqualTo(0);
    }

    public function testOeuvreUndefinedCat()
    {
        $this
            ->given($oeuvres = $this->init())
                ->and($oDao = new \Mock\OeuvreDAO)
                ->and($this->calling($oDao)->getOeuvres = $oeuvres)
                ->and($cDao = new \Mock\CategoryDAO)
                ->and($this->calling($cDao)->categoryExists = false)
            ->if($res = \NumberOne\Controller\RankOeuvre::getTop3Oeuvre(new Category("A"),EnumPeriod::$ever, $oDao, $cDao))
                ->then
                ->string($res)
                ->isIdenticalTo(Error::$UndefinedCategory);
    }

}