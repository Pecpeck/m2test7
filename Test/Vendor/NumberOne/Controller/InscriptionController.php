<?php
namespace NumberOne\Controller\tests\units;

use \atoum;
use NumberOne\Controller\Error;


class InscriptionController extends atoum
{
    public function testInscriptionValide ( ) {
       
      $this
            ->given($userdao = new \Mock\UserDAO())
            ->and ($this->calling($userdao)->insertUser=true)
            ->and ($this->calling($userdao)->userExists=false)
            ->and($myarray = array ( "pseudo"=>"jack",
                                    "password"=>"mypassword"
                                    )
                 )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
                ->string($myObject)
                    ->isEqualTo(Error::$NoError)
            ;
    }

    public function testInscriptionPseudoVide ( ) {
       
        $this
              ->given($userdao = new \Mock\UserDAO())
              
              ->and($myarray = array ( "pseudo"=>"",
                                      "password"=>"mypassword"
                                      )
                   )
              ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
              ->then
                  ->string($myObject)
                      ->isEqualTo(Error::$EmptyPseudo)
              ;
    }

    public function testInscriptionPseudoCourt ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())

            ->and($myarray = array ( "pseudo"=>"car", // 3 caractères
                "password"=>"mypassword"
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPseudo)
        ;
    }

    public function testInscriptionPseudoLong ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())

            ->and($myarray = array ( "pseudo"=>"ilya17caractèress", // 17 caractères
                "password"=>"mypassword"
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPseudo)
        ;
    }

    public function testInscriptionPseudoMauvaisCaractere ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())

            ->and($myarray = array ( "pseudo"=>"un espace", // caractère spécial
                "password"=>"mypassword"
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPseudo)
        ;
    }

    public function testInscriptionPasswordVide ( ) {

        $this
              ->given($userdao = new \Mock\UserDAO())
              ->and($myarray = array ( "pseudo"=>"abdellah",
                                      "password"=>""
                                      )
                   )
              ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
              ->then
                  ->string($myObject)
                      ->isEqualTo(Error::$EmptyPassword)
              ;
    }

    public function testInscriptionPasswordCourt ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($myarray = array ( "pseudo"=>"abdellah",
                "password"=>"azertyu" // 7 caractères
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPassword)
        ;
    }

    public function testInscriptionPasswordLong ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($myarray = array ( "pseudo"=>"abdellah",
                "password"=>"ilfaut50caractèresdanscemotdepasseetcesttrestreslong" // 51 caractères
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPassword)
        ;
    }

    public function testInscriptionPasswordMauvaisCaractere ( ) {

        $this
            ->given($userdao = new \Mock\UserDAO())
            ->and($myarray = array ( "pseudo"=>"abdellah",
                "password"=>"un espace" // caractère spécial
            )
            )
            ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
            ->then
            ->string($myObject)
            ->isEqualTo(Error::$SyntaxPassword)
        ;
    }

    public function testInscriptionPseudoExisteDeja ( ) {

        $this
              ->given($userdao = new \Mock\UserDAO())
              ->and($this->calling($userdao)->userExists=TRUE )
              ->and($myarray = array ( "pseudo"=>"bentaleb",
                                      "password"=>"fdfdffdfdf"
                                      )
                   )
              ->if($myObject = \NumberOne\Controller\InscriptionController::createUser($userdao,$myarray["pseudo"],$myarray["password"]))
              ->then
                  ->string($myObject)
                      ->isEqualTo(Error::$ExistingPseudo)

                      ;
    }

}
